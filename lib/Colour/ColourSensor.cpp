#include <Arduino.h>
#include <Adafruit_TCS34725.h>
#include "ColourSensor.h"


void ColourSensor::populateRGB(uint8_t &r, uint8_t &g, uint8_t &b) {
  _tcs.setInterrupt(false);
  delay(60);
  r = _tcs.read8(TCS34725_RDATAL);
  g = _tcs.read8(TCS34725_GDATAL);
  b = _tcs.read8(TCS34725_BDATAL);
  _tcs.setInterrupt(true);
}

void ColourSensor::populateRGB(colour_t &colour) {
  uint8_t r, g, b;
  populateRGB(r, g, b);
  colour.r = r;
  colour.g = g;
  colour.b = b;
}

