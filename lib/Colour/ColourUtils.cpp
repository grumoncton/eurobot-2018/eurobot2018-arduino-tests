#include "ColourUtils.h"

uint32_t colourDistance(colour_t &first, colour_t &second) {
  int16_t r = (int16_t) first.r - (int16_t) second.r;
  int16_t g = (int16_t) first.g - (int16_t) second.g;
  int16_t b = (int16_t) first.b - (int16_t) second.b;
  return sqrt(pow(r, 2) + pow(g, 2) + pow(b, 2));
}

uint8_t findClosestColour(colour_t &from) {
  uint32_t min = (uint32_t) - 1;
  uint8_t index;

  for (uint8_t i = 0; i < 5; i++) {
    uint32_t current = colourDistance(from, blockDefinitions[i]);

    if (current < min) {
      min = current;
      index = i;
    }
  }

  return index;
}


uint8_t readColourSensorInStack(ColourSensor &sensor, uint8_t position) {
  for (uint8_t i = 0; i < 5; i++) {
    digitalWrite(colourSensorVinPins[i], i == position);
  }
  // sensor._tcs.begin();

  colour_t colour;
  sensor.populateRGB(colour);

  return findClosestColour(colour);
}

uint8_t findColourInStack(ColourSensor &sensor, uint8_t colour) {
  for (uint16_t i = 0; i < 5; i++) {
    if (readColourSensorInStack(sensor, i) != i) continue;

    return i;
  }

  return -1;
}

