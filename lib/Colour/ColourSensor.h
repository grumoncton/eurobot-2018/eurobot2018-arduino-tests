#include <Arduino.h>
#include <Adafruit_TCS34725.h>
#include "ColourType.h"

#ifndef ColourSensor_h
#define ColourSensor_h

class ColourSensor {
  public:
  Adafruit_TCS34725 _tcs;
  void populateRGB(uint8_t &r, uint8_t &g, uint8_t &b);
  void populateRGB(colour_t &colour);
};

#endif


