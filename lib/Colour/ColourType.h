#ifndef ColourType_h
#define ColourType_h

#include <Arduino.h>

typedef struct {
  uint16_t r, g, b;
} colour_t;

#endif

