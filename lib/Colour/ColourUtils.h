#include <Arduino.h>
#include "ColourSensor.h"

#ifndef ColourUtils_h
#define ColourUtils_h

const uint8_t colourSensorVinPins[5] = { 8, 9, 3, 4, 5 };

static colour_t blockDefinitions[5] = {
  // Black
  {   0,   0,   0 },
  // Orange
  { 218, 113,  52 },
  // Green
  {  97, 153,  58 },
  // Yellow
  { 247, 180,   0 },
  // Blue
  {   0,  90, 139 },
};

uint32_t colourDistance(colour_t &a, colour_t &b);
uint8_t readColourSensorInStack(ColourSensor &sensor, uint8_t position);
uint8_t findClosestColour(colour_t & from);

#endif

