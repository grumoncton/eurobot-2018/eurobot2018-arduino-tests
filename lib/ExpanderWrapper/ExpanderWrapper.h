#include <Arduino.h>
;

#ifndef ExpanderWrapper_h
#define ExpanderWrapper_h

class ExpanderWrapper {
  private:
  uint8_t _state;
  uint8_t _address;
  public:
  ExpanderWrapper(uint8_t address);
  bool read(uint8_t pin);
  void write(uint8_t pin, bool status);
};

#endif

