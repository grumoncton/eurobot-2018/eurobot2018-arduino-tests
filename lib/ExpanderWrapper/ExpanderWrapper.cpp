#include <Arduino.h>
#include <Wire.h>
#include "ExpanderWrapper.h"
;

ExpanderWrapper::ExpanderWrapper(uint8_t address) {
  _address = address;
}

void ExpanderWrapper::write(uint8_t pin, bool status) {
  _state = bitWrite(_state, pin, status);

  Wire.beginTransmission(_address);
  Wire.write(_state);
  Wire.endTransmission();
}

bool ExpanderWrapper::read(uint8_t pin) {
  return bitRead(_state, pin);
}

