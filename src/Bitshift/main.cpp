#include <Arduino.h>

//Test position values
double XPosExt = -1250.75;
double YPosExt = 475.36;
double APosExt = 0.15784;

//Byte array to be sent via I2C
byte ActualPosByte[12];

//Received arrays (simulating PI)
uint32_t ReceivedPosByte[3];
double ReceivedPosDouble[3];

bool State = true;

void setup()
{
    Serial.begin(9600);
}

void loop()
{
    while (State)
    {
        //Convert doubles to unsigned ints, because bitshift cannot be executed on negatives and/or decimals
        uint32_t XPosExtMult = 100 * abs(XPosExt);
        uint32_t YPosExtMult = 100 * abs(YPosExt);
        uint32_t APosExtMult = 100000 * abs(APosExt);

        //Bitshift for XPosExt first byte
        //Place MSB = 1 (flag signifying negative number)
        if (XPosExt < 0)
        {
            ActualPosByte[0] = ((XPosExtMult >> 24) & 0xFF) | 0x80;
        }
        else
        {
            ActualPosByte[0] = (XPosExtMult >> 24) & 0xFF;
        }
        ActualPosByte[1] = (XPosExtMult >> 16) & 0xFF;  //Bitshift for XPosExt second byte
        ActualPosByte[2] = (XPosExtMult >> 8) & 0xFF;   //Bitshift for XPosExt third byte
        ActualPosByte[3] = XPosExtMult & 0xFF;  //Bitshift for XPosExt fourth byte


        //Bitshift for YPosExt first byte
        //Place MSB = 1 (flag signifying negative number)
        if (YPosExt < 0)
        {
            ActualPosByte[4] = ((YPosExtMult >> 24) & 0xFF) | 0x80;
        }
        else
        {
            ActualPosByte[4] = (YPosExtMult >> 24) & 0xFF;
        }
        ActualPosByte[5] = (YPosExtMult >> 16) & 0xFF;  //Bitshift for YPosExt second byte
        ActualPosByte[6] = (YPosExtMult >> 8) & 0xFF;   //Bitshift for YPosExt third byte
        ActualPosByte[7] = YPosExtMult & 0xFF;  //Bitshift for YPosExt fourth byte

        //Bitshift for APosExt first byte
        //Place MSB = 1 (flag signifying negative number)
        if (APosExt < 0)
        {
            ActualPosByte[8] = ((APosExtMult >> 24) & 0xFF) | 0x80;
        }
        else
        {
            ActualPosByte[8] = (APosExtMult >> 24) & 0xFF;
        }
        ActualPosByte[9] = (APosExtMult >> 16) & 0xFF;  //Bitshift for APosExt second byte
        ActualPosByte[10] = (APosExtMult >> 8) & 0xFF;  //Bitshift for APosExt third byte
        ActualPosByte[11] = APosExtMult & 0xFF; //Bitshift for APosExt fourth byte

        //Convert receiving values (simulating PI to verify sent bytes)
        ReceivedPosByte[0] = (((uint32_t)ActualPosByte[0] << 24) & 0x7F) | ((uint32_t)ActualPosByte[1] << 16) | ((uint32_t)ActualPosByte[2] << 8) | (uint32_t)ActualPosByte[3]; //Remove negative flag and bitshift to find XPosExt
        ReceivedPosByte[1] = (((uint32_t)ActualPosByte[4] << 24) & 0x7F) | ((uint32_t)ActualPosByte[5] << 16) | ((uint32_t)ActualPosByte[6] << 8) | (uint32_t)ActualPosByte[7]; //Remove negative flag and bitshift to find YPosExt
        ReceivedPosByte[2] = (((uint32_t)ActualPosByte[8] << 24) & 0x7F) | ((uint32_t)ActualPosByte[9] << 16) | ((uint32_t)ActualPosByte[10] << 8) | (uint32_t)ActualPosByte[11];   //Remove negative flag and bitshift to find APosExt

        if (ActualPosByte[0] >> 7)  //Convert to double by adding decimals and add negative sign when indicated by flag for XPosExt
        {
            ReceivedPosDouble[0] = -(ReceivedPosByte[0] / 100.00);
        }
        else
        {
            ReceivedPosDouble[0] = ReceivedPosByte[0] / 100.00;
        }

        if (ActualPosByte[4] >> 7)  //Convert to double by adding decimals and add negative sign when indicated by flag for YPosExt
        {
            ReceivedPosDouble[1] = -(ReceivedPosByte[1] / 100.00);
        }
        else
        {
            ReceivedPosDouble[1] = ReceivedPosByte[1] / 100.00;
        }

        if (ActualPosByte[8] >> 7)  //Convert to double by adding decimals and add negative sign when indicated by flag for APosExt
        {
            ReceivedPosDouble[2] = -(ReceivedPosByte[2] / 100000.000);
        }
        else
        {
            ReceivedPosDouble[2] = ReceivedPosByte[2] / 100000.000;
        }

        //Print values to verify accuracy
        Serial.println(ReceivedPosDouble[0]);
        Serial.println(ReceivedPosDouble[1]);
        Serial.println(ReceivedPosDouble[2], 5);

        State = false;
    }
}
