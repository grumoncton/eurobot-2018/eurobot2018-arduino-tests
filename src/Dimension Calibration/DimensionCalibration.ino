//Recalculate WHEEL_DIAMETER_INT before proceeding

#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

bool state = true;

RoboClaw roboclaw(&Serial3, 10000);

#define COUNT_PER_ROTATION_INT 979.2  //(20.4*48) Internal Encoder ticks per rotation
#define COUNT_PER_ROTATION_EXT 1600  // External Encoder ticks per rotation
#define WHEEL_DIAMETER_EXT 63.4   // External encoder Wheel circumference in mm
#define WHEEL_DIAMETER_INT 66.8   // Internal encoder Wheel circumference in mm
#define WHEEL_SEPARATION_EXT 202.8 // Distance between encoders
#define WHEEL_SEPARATION_INT 287.3 // Distance between encoders
#define ADDRESS 0x80
double mm_per_count_EXT = (WHEEL_DIAMETER_EXT*PI)/COUNT_PER_ROTATION_EXT; // mm/count
double mm_per_count_INT = (WHEEL_DIAMETER_INT*PI)/COUNT_PER_ROTATION_INT; // mm/count

int32_t encL_EXT         =  0;     // position external left encoder
int32_t encR_EXT         =  0;     // position extermal right encoder
int32_t encL_INT         = 0 ;     // position internal left encoder
int32_t encR_INT         = 0 ;     // position intermal right encoder

//Same as MoveTo except distance is measured in wheel rotations in order to test precision and verify CPR
void RollTo(double wheelRotations, double speed, double accel, double deccel)
{
    double accDistance = (speed*speed)/(2*accel);
    double decDistance = (speed*speed)/(2*deccel);
    double distance = wheelRotations*(WHEEL_DIAMETER_INT*PI)
    uint32_t totalAccDistance = accDistance + decDistance;

    if (totalAccDistance > distance)
    {
        accDistance = distance * (accDistance/totalAccDistance);
        decDistance = distance * (decDistance/totalAccDistance);
    }
    else
    {
        accDistance = distance - decDistance;
    }

    int accDistanceQPPS = accDistance / mm_per_count_INT;
    int decDistanceQPPS = decDistance / mm_per_count_INT;
    int speedQPPS = speed / mm_per_count_INT;
    int accelQPPS = accel / mm_per_count_INT;
    int deccelQPPS = deccel / mm_per_count_INT;

    //Try command below with flag = 0
    roboclaw.SpeedAccelDistanceM1M2(ADDRESS, accelQPPS, speedQPPS, accDistanceQPPS, speedQPPS, accDistanceQPPS, 1);
    roboclaw.SpeedAccelDistanceM1M2(ADDRESS, deccelQPPS, speedQPPS, decDistanceQPPS, speedQPPS, decDistanceQPPS, 1);
    //SpeedAccelDistanceM1M2(uint8_t address, uint32_t accel, uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag=0);
    Serial.print("Motor 1 has completed");
    Serial.print(roboclaw.ReadEncM1()/COUNT_PER_ROTATION_INT);
    Serial.println(" rotations");
    Serial.print("Motor 2 has completed");
    Serial.print(roboclaw.ReadEncM2()/COUNT_PER_ROTATION_INT);
    Serial.println(" rotations");

    //Uncomment below to test second mixed command
    // roboclaw.SpeedAccelDistanceM1M2_2(ADDRESS, accelQPPS, speedQPPS, accDistanceQPPS, accelQPPS, speedQPPS, accDistanceQPPS, 1)
    // roboclaw.SpeedAccelDistanceM1M2_2(ADDRESS, deccelQPPS, speedQPPS, decDistanceQPPS, deccelQPPS, speedQPPS, decDistanceQPPS, 1)
    //SpeedAccelDistanceM1M2_2(uint8_t address, uint32_t accel1, uint32_t speed1, uint32_t distance1, uint32_t accel2, uint32_t speed2, uint32_t distance2, uint8_t flag=0);
}

void MoveTo(double distance, double speed, double accel, double deccel)
{
    double accDistance = (speed*speed)/(2*accel);
    double decDistance = (speed*speed)/(2*deccel);
    uint32_t totalAccDistance = accDistance + decDistance;

    if (totalAccDistance > distance)
    {
        accDistance = distance * (accDistance/totalAccDistance);
        decDistance = distance * (decDistance/totalAccDistance);
    }
    else
    {
        accDistance = distance - decDistance;
    }

    int accDistanceQPPS = accDistance / mm_per_count_INT;
    int decDistanceQPPS = decDistance / mm_per_count_INT;
    int speedQPPS = speed / mm_per_count_INT;
    int accelQPPS = accel / mm_per_count_INT;
    int deccelQPPS = deccel / mm_per_count_INT;

    roboclaw.SpeedAccelDistanceM1M2(ADDRESS, accelQPPS, speedQPPS, accDistanceQPPS, speedQPPS, accDistanceQPPS, 1);
    roboclaw.SpeedAccelDistanceM1M2(ADDRESS, deccelQPPS, speedQPPS, decDistanceQPPS, speedQPPS, decDistanceQPPS, 1);
}

void setup()
{
    Serial.begin(38400);
    Serial3.begin(38400);
    roboclaw.begin(38400);
}

void loop()
{
    if (state == true)
    {
        // MoveTo(1200, 250, 500, 500);
        RollTo(50, 250, 500, 500)
        state = false;
    }
}
