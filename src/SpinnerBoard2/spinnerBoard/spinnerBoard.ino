#include <DRV8825.h>
#include <Servo.h>
#include <Wire.h>

enum Instruction {
  NONE = 0x00,
  SERVO_UP = 0x10,
  SERVO_DOWN = 0x11,
  STEPPER_CW90 = 0x12,
  STEPPER_CCW90 = 0x13,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  STOPPED = 2,
  DONE = 3,
};

volatile Instruction _instruction = NONE;
volatile State _state = READY;

#define SPR (200) 
#define RPM (600) 
#define ENA_PIN (10) 
#define M0 (9)   
#define M1 (8) 
#define M2 (7)
#define STEP_PIN (6) 
#define DIR_PIN (5)
#define MICRO_STEPS (32)

DRV8825 stepper(SPR, DIR_PIN, STEP_PIN, ENA_PIN, M0, M1, M2);
Servo servo1;
Servo servo2;

uint8_t ver1 = 135; uint8_t ver2 = 45;  //Vertical servo position
uint8_t hor1 = 45; uint8_t hor2 = 135;  //Horizontal servo position

void setup() {
  Wire.begin(0x31);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);
  
  stepper.begin(RPM,MICRO_STEPS);
  stepper.disable();
  
  servo1.attach(11);  //Servo 1 on pin 11
  servo2.attach(12);  //Servo 2 on pin 12
  servo1.write(hor1);
  servo2.write(hor2);
}

void loop() { 
  if (_state == WORKING) {
    switch (_instruction) {
      case SERVO_UP:
        servo1.write(135);
        servo2.write(45);                  
        _state = READY;
        break;
        
      case SERVO_DOWN:        
        for (int pos1 = 135; pos1 >= 45; pos1--) {
          servo1.write(pos1);              
          servo2.write(180-pos1);
          delay(8);                       
        }
        _state = READY;
        break;
        
      case STEPPER_CW90:
        stepper.enable();
        stepper.rotate(1270);
        //stepper.disable();
        _state = READY;
        break;
        
      case STEPPER_CCW90:
        stepper.enable();
        stepper.rotate(-1270);
        //stepper.disable();
        _state = READY;
        break;   
    }
  }
}

void dataReceive() {
  _instruction = static_cast<Instruction>(Wire.read());
  _state = WORKING;

}

void dataRequest() {
  Wire.write(_state);
}

