#include <Wire.h>

int trigPin = 13;
int echoPin = 12;
long duration;
int distINT;
uint8_t distBYTE;

uint8_t dist[] = {0,80,95,45,80,60};

void setup() {
  // put your setup code here, to run once:
Wire.begin(8);
Serial.begin(9600);
pinMode(13,OUTPUT);
pinMode(12,INPUT);
Wire.onRequest(dataRequest);
}

void loop (){
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH,3000);
    distINT = (duration / 2) / 2.91;
    Serial.println(distINT);
    if (distINT > 255) {
      distINT = 0;
    }
    distBYTE = (byte) distINT;
}

void dataRequest() {
    Wire.write(distBYTE);
    Wire.write(distBYTE+1);
}
