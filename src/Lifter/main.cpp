#include <DRV8825.h>
#include <Wire.h>

#define SPR (200)  //Steps per revolution
#define RPM (500) 
#define ENA_PIN (10) 
#define M0 (9)     
#define M1 (8)       
#define M2 (7)          
#define STEP_PIN (6)     
#define DIR_PIN (5)   
#define MICRO_STEPS (32) 

DRV8825 lifter(SPR, DIR_PIN, STEP_PIN, ENA_PIN, M0, M1, M2);

#define RAISE_HEIGHT_1  = 63;
#define DROP_HEIGHT_1   = 58;
#define RAISE_HEIGHT_2  = RAISE_HEIGHT_1  + 58;
#define DROP_HEIGHT_2   = DROP_HEIGHT_1   + 58;
#define RAISE_HEIGHT_3  = RAISE_HEIGHT_2  + 58;
#define DROP_HEIGHT_3   = DROP_HEIGHT_2   + 58;
#define RAISE_HEIGHT_4  = RAISE_HEIGHT_3  + 58;
#define DROP_HEIGHT_4   = DROP_HEIGHT_3   + 58;
#define RAISE_HEIGHT_5  = RAISE_HEIGHT_4  + 58;
#define DROP_HEIGHT_5   = DROP_HEIGHT_4   + 58;

enum Instruction {
  NONE    = 0x00,
  HOME    = 0x01,
  RAISE_OVER_1 = 0x10,
  DROP_OFF_1   = 0x11,
  RAISE_OVER_2 = 0x20,
  DROP_OFF_2   = 0x21,
  RAISE_OVER_3 = 0x30,
  DROP_OFF_3   = 0x31,
  RAISE_OVER_4 = 0x40,
  DROP_OFF_4   = 0x41,
  RAISE_OVER_5 = 0x50,
  DROP_OFF_5   = 0x51,  
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  STOPPED = 2,
  DONE = 3,
};

volatile Instruction _instruction = NONE;
volatile State _state = READY;

#define HOME_HEIGHT (52)
int currentHeight;

void setup() {
  Wire.begin(0x30);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);
   
  pinMode(12,INPUT);		//Limit switch to home stepper
  digitalWrite(12,HIGH);	//Use pull-up resistor
  
  lifter.begin(RPM,MICRO_STEPS);
  lifter.disable();
  
  Serial.begin(9600);
}

void loop() {
  if (_state == WORKING) {
    switch (_instruction) {
      case HOME :
        home();
        break;
        
      case RAISE_OVER_1 :
        move(RAISE_HEIGHT_1 - currentHeight);
        break;
      case DROP_OFF_1 :
        move(DROP_HEIGHT_1 - currentHeight);
        break;
    
      case RAISE_OVER_2 :
        move(RAISE_HEIGHT_2 - currentHeight);
        break;
      case DROP_OFF_2 :
        move(DROP_HEIGHT_2 - currentHeight);
        break;
        
      case RAISE_OVER_3 :
        move(RAISE_HEIGHT_3 - currentHeight);
        break;
      case DROP_OFF_3 :
        move(DROP_HEIGHT_3 - currentHeight);
        break;
        
      case RAISE_OVER_4 :
        move(RAISE_HEIGHT_4 - currentHeight);
        break;
      case DROP_OFF_4 :
        move(DROP_HEIGHT_4 - currentHeight);
        break;
        
      case RAISE_OVER_5 :
        move(RAISE_HEIGHT_5 - currentHeight);
        break;
      case DROP_OFF_5 :
        move(DROP_HEIGHT_5 - currentHeight);
        break;
    }
  }
}

void home() {
  lifter.enable();
  while (digitalRead(12) == HIGH) {
    lifter.rotate(-5);
  }
  currentHeight = HOME_HEIGHT;
  //Serial.print("Spinner Height : "); Serial.println(currentHeight);
  lifter.stop();
  lifter.disable();
}

void move(int movement) {
  lifter.enable();
  lifter.rotate(-movement*45.4545);
  currentHeight = currentHeight - movement;
  //Serial.print("Spinner Height : "); Serial.println(currentHeight);
  lifter.stop();
  lifter.disable();
}

void dataReceive() {
  _instruction = static_cast<Instruction>(Wire.read());
  _state = WORKING;
}

void dataRequest() {
  Wire.write(_state);
}



