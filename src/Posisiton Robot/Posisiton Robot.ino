#include <Arduino.h>
#include <RoboClaw.h>
//#include <SimpleTimer.h>
#include <math.h>


//SoftwareSerial serial(10,11);  // rx tx
RoboClaw roboclaw(&Serial3, 10000);
//SimpleTimer timer;

#define COUNT_PER_ROTATION_INT 979.2  //(20.4*48) Internal Encoder ticks per rotation (maybe 979 instead?)
#define COUNT_PER_ROTATION_EXT 1600  // External Encoder ticks per rotation
#define WHEEL_DIAMETER_EXT 63.4   // External encoder Wheel circumference in mm
#define WHEEL_DIAMETER_INT 64.7   // Internal encoder Wheel circumference in mm
#define WHEEL_SEPARATION_EXT 202.8 // Distance between encoders
#define WHEEL_SEPARATION_INT 287.3 // Distance between encoders
#define ADDRESS 0x80
double mm_per_count_EXT = (WHEEL_DIAMETER_EXT*PI)/COUNT_PER_ROTATION_EXT; // mm/count
double mm_per_count_INT = (WHEEL_DIAMETER_INT*PI)/COUNT_PER_ROTATION_INT; // mm/count

// Positions in mm and degrees
#define X_START 0
#define Y_START 0
#define A_START 0

double    xPos      =  X_START; // xPos is the x position at the moment
double    yPos      =  Y_START; // yPos is the y position at the moment
double    anglePos  =  A_START; // Angle in degrees at the moment
double    anglePosRad = anglePos * PI / 180; // Angle in radians

double    xDest      =  0; // in mm
double    xDest_Count_INT =  xDest; // xDest in counts for roboclaw (converts mm to counts)
double    xDest_Count_EXT =  xDest; // xDest in counts for External encoders (converts mm to counts)
double    yDest      =  0; // in mm
double    yDest_Count_INT =  yDest; // xDest in counts for roboclaw (converts mm to counts)
double    yDest_Count_EXT =  yDest; // xDest in counts for External encoders (converts mm to counts)
double    Dest       =  sqrt(sq(xDest) + sq(yDest)) ; //in mm
double    Dest_Count_INT ;// Dest in counts for roboclaw (converts mm to counts)
double    Dest_Count_EXT ; // Dest in counts for external encoders (converts mm to counts)
double    angleDest  =  anglePos ; // in degrees

int32_t encL_EXT         =  0;     // position external left encoder
int32_t encR_EXT         =  0;     // position extermal right encoder
int32_t encL_INT         = 0 ;     // position internal left encoder
int32_t encR_INT         = 0 ;     // position intermal right encoder

//Cette fonction peut printer des variables "double/float"
void printDouble( double val, unsigned int precision){
// prints val with number of decimal places determine by precision
// NOTE: precision is 1 followed by the number of zeros for the desired number of decimial places
// example: printDouble( 3.1415, 100); // prints 3.14 (two decimal places)

   Serial.print (int(val));  //prints the int part
   Serial.print("."); // print the decimal point
   unsigned int frac;
   if(val >= 0)
       frac = (val - int(val)) * precision;
   else
       frac = (int(val)- val ) * precision;
   Serial.println(frac,DEC) ;
}

// set internal encoders to 0
void Set2ZERO(void){
  Serial.println("Set to Zero!");
  roboclaw.SetEncM1(ADDRESS, 0);
  roboclaw.SetEncM2(ADDRESS, 0);
    delay(1000); // Ca seem comme si il a besoin d<un delay pour mettre la position a 0
}

int vel1 = 2000; // make sure speed is well bellow the qpps value if motor wont move. You can also try reducing acc.
int vel2 = vel1; // vel1 correspond au moteur de droite et vel2 correspond au moteur de gauche pour maintenant.
int acc = 2000;
int dec = acc;
int dis = 2000; //Il faut préciser les distance maximales et minimales dans ionmotion (positif et négatif)

// La fonction Turn est seulement pour tourner
// angle : l'angle pour tourner. Dir : la direction pour tourner
// On a donner comme convention 1 virage à gauche et 0 virage à droite pour Dir
void Turn(double angle, bool Dir){
  double dist_mm ;
  int dist_count_INT;
  dist_mm = WHEEL_SEPARATION_INT*PI*(angle/(360)); // divisé par deux car on veut que le robot reste sur place. Pour cela un moteur va en avant et l'autre vers l'arrière
  dist_count_INT = dist_mm/mm_per_count_INT;
  // Maintenant si Dir=0 on tourne à droite et si Dir=1 on tourne à gauche
  if(Dir == 0){
    // Un moteur va en avant et l'autre vers l'arrière. Regarde "dist_count_INT"
     roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, acc, vel1, dec, -dist_count_INT, acc, vel2, dec, dist_count_INT, 1);
     }
  else{
    // Un moteur va en avant et l'autre vers l'arrière. Regarde "dist_count_INT"
     roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, acc, vel1, dec, dist_count_INT, acc, vel2, dec, -dist_count_INT, 1);
     }
  }


//MoveTo tourne first et then il avance
void MoveTo(double x, double y, bool direction = 1){
  xDest = x - xPos;
  yDest = y - yPos;
  Dest  = sqrt(sq(xDest) + sq(yDest)) ;
  angleDest = (atan2 (yDest, xDest )) * 180 / PI;
  double diff_angleDest = angleDest - anglePos;
  bool Turn_Dir; // le sens de direction 1 est Droite et 0 est gauche


  // On doit tourner en premier
 // Si diff_angleDest=0, on ne tourne pas   }
  // Si diff_angleDest!=0, il faut tourner
  if(diff_angleDest!=0){
      // On corrige l'angle entre 0 et 180
      // On veut tourner dans la direction la plus courte
      // Ensuite on appelle une fonction pour tourner
      if (abs(diff_angleDest) > 180){
        if (diff_angleDest > 0){
          diff_angleDest = diff_angleDest - 180;
          Turn_Dir = 0 ; // Virage à droite // Sens de l'aiguille d'une horloge
          Turn(diff_angleDest,Turn_Dir); // Fonction pour tourner
          }
        else{
          diff_angleDest = 180 + diff_angleDest;
          Turn_Dir = 1  ; // Virage à gauche // Contre de l'aiguille d'une horloge
          Turn(diff_angleDest,Turn_Dir); // Fonction pour tourner
          }
        }
      // Si l'angle est moins de 180
      else{
        if (diff_angleDest > 0){
          Turn_Dir = 1 ; // Virage à gauche  // Contre l'aiguille d'une horloge
          Turn(diff_angleDest,Turn_Dir); // Fonction pour tourner
          }
        else{
          Turn_Dir = 0  ; // Virage à droite // Sens de l'aiguille d'une horloge
          Turn(diff_angleDest, Turn_Dir); // Fonction pour tourner
          }
        }
  }

  // ON MET LES ENCODEURS INTERNES À 0
  //roboclaw.DutyM1M2(ADDRESS, 0, 0); // Pour s'assurer d'arrêter de consommer du rcourrant si le mouvement est terminé
  Set2ZERO();


  // Maintenant on peut maintenant avancer ou reculer le robot
  if(Direction == 1){ // Si Direction = 1 le robot va en avant
      Serial.println("On va en avant!");
      roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, acc, vel1, dec, Dest_count_INT, acc, vel2, dec, Dest_count_INT, 0);
      }
  else{ // On tourne en arrière Si Direction est 0
      Serial.println("On va en arrière!");
      roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, acc, vel1, dec, -Dest_count_INT, acc, vel2, dec, -Dest_count_INT, 0);
      }

  // ON MET LES ENCODEURS INTERNES À 0
  Set2ZERO();
  //roboclaw.DutyM1M2(ADDRESS, 0, 0); // Pour s'assurer d'arrêter de consommer du rcourrant si le mouvement est terminé
  //On met 'a jour les positions x et y et angle
  xPos = x;
  yPos = y;
  anglePos = angleDest ;

  }



void setup(){
  Serial.begin(38400);
  Serial3.begin(38400);
  roboclaw.begin(38400);

  //timer.setInterval(1, computePosition);

//  //TIMER SETUP
//  cli();
//  //set timer1 interrupt at 1KHz
//  TCCR1A = 0;
//  TCCR1B = 0;
//  TCNT1 = 0;
//  OCR1A = 1999;// (16MHz)/(1000*8)-1
//  TCCR1B |= (1 << WGM12);
//  TCCR1B |= (1 << CS11);
//  TIMSK1 |= (1 << OCIE1A);
//  sei();
//  //END TIMER SETUP
  Serial.println("Ready");
}



void loop(){
 // timer.run();
 MoveTo(500,0,1);
 delay(2000);
 MoveTo(X_START,Y_START,1);
 delay(2000);

}
