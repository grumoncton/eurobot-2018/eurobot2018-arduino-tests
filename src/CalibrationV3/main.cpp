#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void Distance(double distance);
void Rotate(double rotations);
void UpdateExtEncoder();

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial2, 10000);
#define ADDRESS 0x80 //Packet Serial Mode address byte (Mode 7 on RoboClaw)

//Setting up communication with XBee
#define XBee Serial3

//Primary Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
#define WHEEL_DIA_INT 50.15 //Internal encoder wheel diameter (mm)
#define WHEEL_SEP_INT 163.0 //Distance between internal encoder centers (mm)
#define CPR_EXT 1600 //External encoder ticks per rotation
#define WHEEL_DIA_EXT 50.0 //External encoder wheel diameter (mm)
#define WHEEL_SEP_EXT 116.0 //Distance between external encoder centers (mm) Decrease if undershoot

//Secondary Robot Dimensions
// #define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
// #define WHEEL_DIA_INT 50.20 //Internal encoder wheel diameter (mm)
// #define WHEEL_SEP_INT 190.6 //Distance between internal encoder centers (mm)
// #define CPR_EXT 1600 //External encoder ticks per rotation
// #define WHEEL_DIA_EXT 50.0 //External encoder wheel diameter (mm)
// #define WHEEL_SEP_EXT 106.5 //Distance between external encoder centers (mm) Decrease if undershoot

double MM_PER_CNT_INT = (WHEEL_DIA_INT*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count

double IntDist;

//Movement quantities speed in mm/s and accel in mm/s^2
#define SPEED 600.0
#define ACCEL 400.0
#define DECEL 400.0

//Movement quantities converted speed in counts/s and accel in counts/s^2
double Speed = SPEED/MM_PER_CNT_INT;
double Accel = ACCEL/MM_PER_CNT_INT;
double Decel = DECEL/MM_PER_CNT_INT;

//External Encoder Parameters
int EncoderPinL1 = 2; //Left encoder's green wire
int EncoderPinL2 = 3; //Left encoder's white wire
int EncoderPinR1 = 18; //Right encoder's white wire
int EncoderPinR2 = 19; //Right encoder's green wire
volatile int LastEncodedL;
volatile int LastEncodedR;
volatile long EncoderValueL;
volatile long EncoderValueR;
long EncoderValueLRead; //Current reading variable of the left external encoder
long EncoderValueRRead; //Current reading variable of the right external encoder

//If condition within main() only runs once if set to false afterwards
bool State = true;

int UpdateDelay = 100;

void setup()
{
    //Set baud rate to be communicated with RoboClaw (Must be set in RoboClaw parameters)
    XBee.begin(38400);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
    delay(100); //Always allow 100ms delay after resetting encoders

    //Accelerometer pin
    pinMode(A0, INPUT);

    //External Encoder Pin Setup
    pinMode(EncoderPinL1, INPUT);
    pinMode(EncoderPinL2, INPUT);
    pinMode(EncoderPinR1, INPUT);
    pinMode(EncoderPinR2, INPUT);
    digitalWrite(EncoderPinL1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinL2, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR2, HIGH); //Turn pullup resistor on
    attachInterrupt(digitalPinToInterrupt(EncoderPinL1), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinL2), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR1), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR2), UpdateExtEncoder, CHANGE);
}

void loop()
{
    if (State)
    {
        Distance(2000);
        //Rotate(rotations in full turns);
        //Rotate(5);
        State = false;
    }
}

void Distance(double distance)
{
    IntDist = distance/MM_PER_CNT_INT;

    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, IntDist, Accel, Speed, Decel, IntDist, 0);

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
        // XBee.print(EncoderValueL);
        // XBee.print("\t");
        // XBee.print(EncoderValueR);
        // XBee.print("!");
    }

    XBee.print("Theoretical distance traveled ");
    XBee.print(distance);
    XBee.print("!");

    XBee.print("Left external encoder counts ");
    XBee.print(EncoderValueL);
    XBee.print("!");

    XBee.print("Right external encoder counts ");
    XBee.print(EncoderValueR);
    XBee.print("!");

    double MeasuredDistance = ((((double)EncoderValueL + (double)EncoderValueR) / 2) / CPR_EXT) * PI * WHEEL_DIA_EXT;
    XBee.print("Distance traveled based on external encoder ");
    XBee.print(MeasuredDistance);
    XBee.print("!");
}

void Rotate(double rotations)
{
    IntDist = (PI * WHEEL_SEP_INT * rotations) / MM_PER_CNT_INT;
    double measuredExtRotations;
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, 0, Accel, Speed, Decel, 0, 0);
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, IntDist, Accel, Speed, Decel, -IntDist, 0);

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
    }

    delay(100);

    measuredExtRotations = (((abs(EncoderValueL) + abs(EncoderValueR)) / 2) * MM_PER_CNT_EXT) / (PI * WHEEL_SEP_EXT);

    XBee.print("Theoretical rotations accomplished ");
    XBee.print(rotations);
    XBee.print("!");

    XBee.print("Rotations based on external encoders ");
    XBee.print(measuredExtRotations, 4);
    XBee.print("!");
}

void UpdateExtEncoder()
{
    int msb_L = digitalRead(EncoderPinL1); //msb_L = most significant bit left motor
    int lsb_L = digitalRead(EncoderPinL2); //lsb_L = least significant bit left motor
    int msb_R = digitalRead(EncoderPinR1); //msb_R = most significant bit right motor
    int lsb_R = digitalRead(EncoderPinR2); //lsb_R = least significant bit right motor

    int encodedL = (msb_L << 1) |lsb_L; //converting the 2 pin of the left motor value to single number
    int encodedR = (msb_R << 1) |lsb_R; //converting the 2 pin of the right motor value to single number
    int sumML  = (LastEncodedL << 2) | encodedL; //adding it to the previous encoded value for left motor
    int sumMR  = (LastEncodedR << 2) | encodedR; //adding it to the previous encoded value for right motor

    if(sumML == 0b1101 || sumML == 0b0100 || sumML == 0b0010 || sumML == 0b1011) EncoderValueL ++; //Left encoder forward motion
    if(sumML == 0b1110 || sumML == 0b0111 || sumML == 0b0001 || sumML == 0b1000) EncoderValueL --; //Left encoder backwards motion
    if(sumMR == 0b1101 || sumMR == 0b0100 || sumMR == 0b0010 || sumMR == 0b1011) EncoderValueR ++; //Right encoder forward motion
    if(sumMR == 0b1110 || sumMR == 0b0111 || sumMR == 0b0001 || sumMR == 0b1000) EncoderValueR --; //Right encoder Backwards motion

    //Save encoder values for next update
    LastEncodedL = encodedL;
    LastEncodedR = encodedR;
}