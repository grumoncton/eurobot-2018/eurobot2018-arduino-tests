/*
Test pour le MMA7361L Module 3 axis accelerometer

 The circuit:
 analog 0: accelerometer self test
 analog 1: z-axis
 analog 2: y-axis
 analog 3: x-axis
 GND : ground
 3.3V : VCC


*/

// these constants describe the pins. They won't change:
#include <Arduino.h>
#include "math.h"

int accelx;
int accely;
int accelz;
int jerkx;
int jerky;
int jerkz;
int lastAccelx;
int lastAccely;
int lastAccelz;

void setup() {

  Serial.begin(9600);

pinMode(A1, INPUT);
pinMode(A2, INPUT);
pinMode(A3, INPUT);
}

void loop() {

accelz = analogRead(A1);
accely = analogRead(A2);
accelx = analogRead(A3);

jerkx = abs(accelx-lastAccelx);
jerky = abs(accely-lastAccely);
jerkz = abs(accelz-lastAccelz);



Serial.print("X: ");
Serial.print(accelx);
Serial.print("\t");
Serial.print("Y: ");
Serial.print(accely);
Serial.print("\t");
Serial.print("Z: ");
Serial.print(accelz);
Serial.print("\t");
Serial.print("==========  ");
Serial.print("jerkX: ");
Serial.print(jerkx);
Serial.print("\t");
Serial.print("jerkY: ");
Serial.print(jerky);
Serial.print("\t");
Serial.print("jerkZ: ");
Serial.print(jerkz);
Serial.print("\t");
Serial.println();

lastAccelx= accelx;
lastAccely= accely;
lastAccelz= accelz;


  delay(100);
}
