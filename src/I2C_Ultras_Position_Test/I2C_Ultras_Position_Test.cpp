#include <Arduino.h>
#include <math.h>
//See BareMinimum example for a list of library functions

//Includes required to use Roboclaw library
// #include <SoftwareSerial.h>
#include "RoboClaw.h"
#include <Wire.h>


int a[32];
int vel1 = 1000; // make sure speed is well bellow the qpps value if motor wont move. You can also try reducing acc.
int vel2 = vel1; //vel2max = 7300
int acc = 3000;
int dec = acc;
// int dis = 2000; //Il faut préciser les distance maximales et minimales dans ionmotion (positif et négatif)

//See limitations of Arduino SoftwareSerial
//SoftwareSerial Serial(10, 11); // Rx Tx
void displayspeed(void);
RoboClaw roboclaw(&Serial3, 10000);
#define address 0x80

void dataReceive(int);
void requestEvent();

void setup() {
	//Open Serial and roboclaw Serial ports
	Wire.begin(0x10);
	Serial.begin(38400);
	Serial3.begin(38400);
	roboclaw.begin(38400);
	// Wire.onRequest(dataRequest);
  Wire.onReceive(dataReceive);
	Wire.onRequest(requestEvent);

	roboclaw.ResetEncoders(address);

	Serial.println("Ready");

}

void loop(){
}

void dataReceive(int position) {
  for(int i = 0; i < position; i++){
    a[i] = Wire.read();
		// Serial.print("a[i] = ");Serial.print(a[i]);Serial.print( " a[0] = ");Serial.println(a[0]);
  }
  a[1] = a[1] << 8;
  int posX = a[1] + a[2];
  a[3] = a[3] << 8;
  int posY = a[3] + a[4];
  int dis = sqrt(pow(posX,2)+pow(posY,2));
  Serial.print(posX); Serial.print(", "); Serial.print(posY);Serial.print(", ");Serial.println(dis);
  roboclaw.SpeedAccelDeccelPositionM1M2(address, acc, vel1, dec, dis, acc, vel2, dec, dis, 0);
}

void requestEvent(void){
  uint8_t status3, status4;
  bool  valid3, valid4;
  int32_t speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
  int32_t speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);
	uint8_t depth1, depth2;
  // Serial.print("speed1 = ");Serial.println(speed1);
	// Serial.print("valid3 = ");Serial.println(valid3);
	// Serial.print("speed2!=0 && speed1!=0 && valid3==1 && valid4==1 = ");Serial.println(speed2!=0 && speed1!=0 && valid3==1 && valid4==1);
	roboclaw.ReadBuffers(address, depth1, depth2);
	if(depth1 != 0x80 && depth2 != 0x80)
	{
			Serial.print("depth1 = ");Serial.println(depth1);
			delay(100);
			Wire.write(0);//"Not done Moving"
			Serial.print("depth2 = ");Serial.println(depth2);
	}
	else{
		Wire.write(1);//"Done Moving"
		roboclaw.ResetEncoders(address);
	}
// 	while (speed2!=0 && speed1!=0 && valid3==1 && valid4==1 ) {
//   		Wire.write(0);//"Not done Moving"
//   		int32_t speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
//   		int32_t speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);
// }
}
	// uint8_t status3, status4;
  // bool  valid3, valid4;
  // int32_t speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
  // int32_t speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);
  //
	// Serial.print("dist = ");Serial.println(dis);
  // roboclaw.SpeedAccelDeccelPositionM1M2(address, acc, vel1, dec, dis, acc, vel2, dec, dis, 0);
	// while (speed2&&speed1!=0) {
	// 		Wire.write("Not done Moving");
	// 		int32_t speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
	// 		int32_t speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);
	//   }
  // displayspeed();
  // delay(2000);
  // roboclaw.SpeedAccelDeccelPositionM1M2(address, acc, vel1, dec, 0, acc, vel2, dec, 0, 0);
  // displayspeed();
  // delay(2000);
	//
  // roboclaw.SpeedAccelDeccelPositionM1M2(address, acc, vel1, dec, dis, acc, vel2, dec, -dis, 0);
  // displayspeed();
  // delay(2000);
  // roboclaw.SpeedAccelDeccelPositionM1M2(address, acc, vel1, dec, 0, acc, vel2, dec, 0, 0);
  // displayspeed();
  // delay(2000);


// void requestEvent() {
// 	int32_t enc1 = roboclaw.ReadEncM1(address, &status1, &valid1);
// 	int32_t enc2 = roboclaw.ReadEncM2(address, &status2, &valid2);
// 	if (valid1&valid2) {
// 	 distBYTE = (byte) encl1+encl2;
// 	 Wire.write(distBYTE);
// 	}
// 	else {
// 		Serial.print("failed ");
// 	}
//
// }

//
// void displayspeed(void)
// {
//   uint8_t status1, status2, status3, status4;
//   bool valid1, valid2, valid3, valid4;
//
//   int32_t enc1 = roboclaw.ReadEncM1(address, &status1, &valid1);
//   int32_t enc2 = roboclaw.ReadEncM2(address, &status2, &valid2);
//   int32_t speed1 = roboclaw.ReadSpeedM1(address, &status3, &valid3);
//   int32_t speed2 = roboclaw.ReadSpeedM2(address, &status4, &valid4);
//   Serial.print("Encoder1:");
//   if (valid1) {
//     Serial.print(enc1, DEC);
//     Serial.print(" ");
//     Serial.print(status1, HEX);
//     Serial.print(" ");
//   }
//   else {
//     Serial.print("failed ");
//   }
//   Serial.print("Encoder2:");
//   if (valid2) {
//     Serial.print(enc2, DEC);
//     Serial.print(" ");
//     Serial.print(status2, HEX);
//     Serial.print(" ");
//   }
//   else {
//     Serial.print("failed ");
//   }
//   Serial.print("Speed1:");
//   if (valid3) {
//     Serial.print(speed1, DEC);
//     Serial.print(" ");
//   }
//   else {
//     Serial.print("failed ");
//   }
//   Serial.print("Speed2:");
//   if (valid4) {
//     Serial.print(speed2, DEC);
//     Serial.print(" ");
//   }
//   else {
//     Serial.print("failed ");
//   }
//   Serial.println();
//
// }
