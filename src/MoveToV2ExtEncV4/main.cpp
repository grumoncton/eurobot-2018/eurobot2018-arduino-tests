#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void MoveTo(double x, double y, double a, bool dir);
void AdjustmentTurn(double deltaA);
void UpdateExtPos();
void UpdateExtEncoder();
void ReadIntEnc();

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial3, 10000);

//Setting up communication with XBee
#define XBee Serial2

//Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
#define CPR_EXT 1600 //External encoder ticks per rotation
#define WHEEL_DIA_INT 64.53 //Internal encoder wheel diameter (mm)
#define WHEEL_DIA_EXT 64.19 //External encoder wheel diameter (mm)
#define WHEEL_SEP_INT 284.3 //Distance between internal encoder centers (mm)
#define WHEEL_SEP_EXT 202.4 //Distance between external encoder centers (mm)
#define ADDRESS 0x80 //Packet Serial Mode address byte (Mode 7 on RoboClaw)
double MM_PER_CNT_INT = (WHEEL_DIA_INT*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count


//Movement quantities speed in mm/s and accel in mm/s^2
#define SPEED 600
#define ACCEL 400
#define DECEL 500

//Starting positions in mmm and degrees
#define X_START 0
#define Y_START 0
#define A_START 0

//Movement quantities converted speed in counts/s and accel in counts/s^2
double Speed = SPEED/MM_PER_CNT_INT;
double Accel = ACCEL/MM_PER_CNT_INT;
double Decel = DECEL/MM_PER_CNT_INT;

//Game variable where 0 is normal and 1 is mirrored along the x axis
bool Game = 0;

//Robot odometry variables
double XPos;
double YPos;
double APos;
double APosRad;

//External Encoder Parameters
int EncoderPinL1 = 2; //Left encoder's green wire
int EncoderPinL2 = 3; //Left encoder's white wire
int EncoderPinR1 = 18; //Right encoder's white wire
int EncoderPinR2 = 19; //Right encoder's green wire
volatile int LastEncodedL;
volatile int LastEncodedR;
volatile long EncoderValueL;
volatile long EncoderValueR;

//Update encoder delay for RoboClaw
int UpdateDelay = 12;

//External Encoder Position Variables
long LastEncoderValueLRead;
long LastEncoderValueRRead;

//Robot odometry variables based on external encoders
double XPosExt;
double YPosExt;
double APosExt;
double APosExtRel;

//If condition within main() only runs once if set to false afterwards
bool State;

bool Collision;

int MaxJerk = 80;

void setup()
{
    //Set baud rate to be communicated with RoboClaw (Must be set in RoboClaw parameters)
    Serial.begin(38400);
    Serial3.begin(38400);
    XBee.begin(9600);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
    delay(100); //Always allow 100ms delay after resetting encoders
    XBee.print("Current Theoretical Position: 0, 0, 0!");
    XBee.print("================================================================================================!");
    XBee.print("Initialized!");
    XBee.print("================================================================================================!");

    //Accelerometer pin
    pinMode(A0, INPUT);

    //External Encoder Pin Setup
    pinMode(EncoderPinL1, INPUT);
    pinMode(EncoderPinL2, INPUT);
    pinMode(EncoderPinR1, INPUT);
    pinMode(EncoderPinR2, INPUT);
    digitalWrite(EncoderPinL1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinL2, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR2, HIGH); //Turn pullup resistor on
    attachInterrupt(digitalPinToInterrupt(EncoderPinL1), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinL2), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR1), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR2), UpdateExtEncoder, CHANGE);

    if (Game) //Reverse all x coordinates, and angles are shifted by PI
    {
        XPos = -X_START;
        if ((A_START >= 0) && (A_START <= 180))
        {
            APos = 180-A_START;
        }
        else if (A_START > 180)
        {
            APos = 360-fabs(180-A_START);
        }
    }
    else
    {
        XPos = X_START;
        APos = A_START;
    }
    YPos = Y_START;
    APosRad = APos*(PI/180); //Convert angle from degrees to radians in order to be valid with math.h library functions
}

void loop()
{
    if (State)
    {
        //MoveTo(x, y, a, start, end, dir);
        State = false;
    }
    MoveTo(1000, 0, -1, 0);
    MoveTo(1000, 1000, -1, 0);
    MoveTo(0, 1000, -1, 0);
    MoveTo(0, 0, 0, 0);
}

//x and y are destination coordinates
//a is the robot's orientation to be adjusted to once the coordinates are reached (-1 for no adjustment angle)
//start decides which motors to be engaged during first AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//end decides which motors to be engaged during second AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//dir set to 0 for forward movement, 1 for backwards movement
void MoveTo(double x, double y, double a, bool dir)
{
    Collision = true;
    while (Collision)
    {
        //Movement variables
        double xDist; //X Distance in mm to be executed during MoveTo command
        double xDistCountInt; //X Distance in counts to be executed during MoveTo command
        double yDist; //Y Distance in mm to be executed during MoveTo command
        double yDistCountInt; //Y Distance in counts to be executed during MoveTo command
        double travelDist; //Distance to be accomplished during MoveTo in counts
        double adjustedA; //Angle to be oriented to in radians
        double deltaA; //Angle between current position and adjustedA in radians
        Collision = false;

        if (Game) //Reverse all x coordinates, and angles are shifted by PI
        {
            x = -x;
            if ((a >= 0) && (a <= 180))
            {
                a = 180-a;
            }
            else if (a > 180)
            {
                a = 360-fabs(180-a);
            }
        }
        double aRad = PI*(a/180);

        xDist = x-XPos;
        xDistCountInt = xDist/MM_PER_CNT_INT;
        yDist = y-YPos;
        yDistCountInt = yDist/MM_PER_CNT_INT;

            //Adjustment angle calculations with resulting angle between 0 and 360 degrees (valid for both games)
            if (((xDist > 0 && yDist >= 0) && (dir == 0)) || ((xDist < 0 && yDist <= 0) && (dir == 1)))
            {
                adjustedA = atan2(fabs(yDist), fabs(xDist));
            }
            else if (((xDist <= 0 && yDist > 0) && (dir == 0)) || ((xDist >= 0 && yDist < 0) && (dir == 1)))
            {
                adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
            }
            else if (((xDist < 0 && yDist <= 0) && (dir == 0)) || ((xDist > 0 && yDist >= 0) && (dir == 1)))
            {
                adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
            }
            else if (((xDist >= 0 && yDist < 0) && (dir == 0)) || ((xDist <= 0 && yDist > 0) && (dir == 1)))
            {
                adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
            }
            else if ((xDist == 0) && (yDist == 0))
            {
                adjustedA = APosRad;
            }

            deltaA = adjustedA-APosRad;

            if (deltaA != 0)
            {
                AdjustmentTurn(deltaA); //First adjustment turn to align robot with destination coordinate
                APosRad = APosExtRel;
                XPos = XPosExt;
                YPos = YPosExt;
                // APosRad = adjustedA;
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
            }

            travelDist = hypot(xDistCountInt, yDistCountInt);

            if (travelDist != 0 && !Collision)
            {
                if (dir == 0) //Forward movement
                {
                    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, travelDist, Accel, Speed, Decel, travelDist, 0);
                }
                else //Backwards movement
                {
                    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, -travelDist, Accel, Speed, Decel, -travelDist, 0);
                }

                //Acceleration variables
                int accel;
                int lastAccel = 336;
                int jerk;

                uint8_t depth1 = 0, depth2 = 0;
                //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
                //Buffer value is the number of commands waiting to be executed by RoboClaw
                //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
                while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
                {
                    roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
                    delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts

                    accel = analogRead(A0);
                    jerk = abs(accel-lastAccel);
                    if (jerk > MaxJerk)
                    {
                        roboclaw.DutyM1M2(ADDRESS, 0, 0);
                        Collision = true;
                        XBee.print("Collision, jerk = ");
                        XBee.print(jerk);
                        XBee.print('!');
                        delay(5000);
                        break;
                    }
                    lastAccel = accel;

                    UpdateExtPos(); //Update robot odometry based on external encoders
                }
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);

                XPos=XPosExt;
                YPos=YPosExt;
                APos = APosExtRel;
                // XPos = x;
                // YPos = y;

                if (a == -1) //No adjustment angle (same as arriving orientation)
                {
                    deltaA = 0;
                }
                else
                {
                    deltaA = aRad-APosRad;
                }

                if (deltaA != 0 && !Collision)
                {
                    AdjustmentTurn(deltaA); //Second adjustment turn to align robot with final angle argument in MoveTo()
                    XPos=XPosExt;
                    YPos=YPosExt;
                    APosRad = APosExtRel;
                    // APosRad = a;
                    roboclaw.ResetEncoders(ADDRESS);
                    delay(100);
                }
                XBee.print("Current Theoretical Position: X ");
                XBee.print(x);
                XBee.print(", Y ");
                XBee.print(y);
                XBee.print(", A ");
                XBee.print(a);
                XBee.print('!');
                XBee.print("Current Position Based on ExtEnc: X ");
                XBee.print(XPosExt);
                XBee.print(", Y ");
                XBee.print(YPosExt);
                XBee.print(", ARel ");
                XBee.print((APosExtRel/PI)*180);
                XBee.print('!');
                XBee.print("------------------------------------------------------------------------------------------------!");
                XBee.print("Position reached!");
                XBee.print("------------------------------------------------------------------------------------------------!");
                if (!Collision)
                {
                    break;
                }
        }
    }
}

void AdjustmentTurn(double deltaA)
{
    double turnDist; //Distance to be accomplished by each wheel while turning

    //Conditions to determine smallest angle to be accomplished (between -PI and PI)
    if (deltaA > PI)
    {
        deltaA -= 2*PI;
    }
    else if (deltaA < -PI)
    {
        deltaA += 2*PI;
    }

    turnDist = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT;

    //Turn achieved by position command with distance arguments having opposite signs to each other
    if (deltaA >= 0) //Positive rotation based on x axis (CCW)
    {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, turnDist, Accel, Speed, Decel, -turnDist, 0);
    }
    else //Negative rotation based on x axis (CW)
    {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, -turnDist, Accel, Speed, Decel, turnDist, 0);
    }

    //Acceleration variables
    int accel;
    int lastAccel = 336;
    int jerk;

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
        accel = analogRead(A0);
        jerk = abs(accel-lastAccel);
        if (jerk > MaxJerk)
        {
            roboclaw.DutyM1M2(ADDRESS, 0, 0);
            Collision = true;
            XBee.print("Collision, jerk = ");
            XBee.print(jerk);
            XBee.print('!');
            delay(5000);
            break;
        }
        lastAccel = accel;

        UpdateExtPos(); //Update robot odometry based on external encoders
    }
}

void UpdateExtPos() //Precision is added the more frequently updated (smaller arcA means smaller subinterval for numeric integration)
{
    double leftDist; //Distance traveled by left motor since last updated
    double rightDist; //Distance traveled by right motor since last updated
    double centerDist; //Distance traveled by the center of the robot since last updated
    double arcA; //Arc accomplished by the robot since last updated where the angle is based on the axle of the robot at starting angle
    long encoderValueLRead; //Current reading variable of the left external encoder
    long encoderValueRRead; //Current reading variable of the right external encoder

    encoderValueLRead = EncoderValueL; //Store left encoder reading as a variable
    encoderValueRRead = EncoderValueR; //Store right encoder reading as a variable
    leftDist = (encoderValueLRead-LastEncoderValueLRead)*MM_PER_CNT_EXT;
    rightDist = (encoderValueRRead-LastEncoderValueRRead)*MM_PER_CNT_EXT;
    centerDist = ((leftDist+rightDist)/2);
    if (Game)
    {
        arcA = (leftDist-rightDist)/WHEEL_SEP_EXT;
    }
    else
    {
        arcA = (rightDist-leftDist)/WHEEL_SEP_EXT;
    }
    if (arcA != 0)
    {
        if (Game)
        {
            XPosExt -= (centerDist/arcA)*(sin(APosExt + arcA)-sin(APosExt));
        }
        else
        {
            XPosExt += (centerDist/arcA)*(sin(APosExt + arcA)-sin(APosExt));
        }
        YPosExt += (centerDist/arcA)*(cos(APosExt)-cos(APosExt+arcA));
    }
    else
    {
        if (Game)
        {
            XPosExt -= centerDist*cos(APosExt); //Approximation valid  for a small arcA
        }
        else
        {
            XPosExt += centerDist*cos(APosExt); //Approximation valid  for a small arcA
        }
        YPosExt += centerDist*sin(APosExt); //Approximation valid  for a small arcA
    }
    APosExt += arcA;

    //Conditions to determine smallest angle
    if (APosExt > PI)
    {
        APosExt = -2*PI+APosExt;
    }
    else if (APosExt < -PI)
    {
        APosExt = 2*PI+APosExt;
    }

    //Convert angle relative to axle to the global coordinates
    if (APosExt < 0)
    {
        APosExtRel = 2*PI+APosExt;
    }
    else
    {
        APosExtRel = APosExt;
    }

    //Shift angles by PI
    if (Game)
    {
        if ((APosExtRel >= 0) && (APosExtRel <= PI))
        {
            APosExtRel = PI-APosExtRel;
        }
        else if (APosExtRel > PI)
        {
            APosExtRel = 2*PI-fabs(PI-APosExtRel);
        }
    }

    //Save encoder values for next update
    LastEncoderValueLRead = encoderValueLRead;
    LastEncoderValueRRead = encoderValueRRead;
}
void UpdateExtEncoder()
{
    int msb_L = digitalRead(EncoderPinL1); //msb_L = most significant bit left motor
    int lsb_L = digitalRead(EncoderPinL2); //lsb_L = least significant bit left motor
    int msb_R = digitalRead(EncoderPinR1); //msb_R = most significant bit right motor
    int lsb_R = digitalRead(EncoderPinR2); //lsb_R = least significant bit right motor

    int encodedL = (msb_L << 1) |lsb_L; //converting the 2 pin of the left motor value to single number
    int encodedR = (msb_R << 1) |lsb_R; //converting the 2 pin of the right motor value to single number
    int sumML  = (LastEncodedL << 2) | encodedL; //adding it to the previous encoded value for left motor
    int sumMR  = (LastEncodedR << 2) | encodedR; //adding it to the previous encoded value for right motor

    if(sumML == 0b1101 || sumML == 0b0100 || sumML == 0b0010 || sumML == 0b1011) EncoderValueL ++; //Left encoder forward motion
    if(sumML == 0b1110 || sumML == 0b0111 || sumML == 0b0001 || sumML == 0b1000) EncoderValueL --; //Left encoder backwards motion
    if(sumMR == 0b1101 || sumMR == 0b0100 || sumMR == 0b0010 || sumMR == 0b1011) EncoderValueR ++; //Right encoder forward motion
    if(sumMR == 0b1110 || sumMR == 0b0111 || sumMR == 0b0001 || sumMR == 0b1000) EncoderValueR --; //Right encoder Backwards motion

    //Save encoder values for next update
    LastEncodedL = encodedL;
    LastEncodedR = encodedR;
}
void ReadIntEnc() //Prints internal encoder readings (do not use in buffer loop as it is very slow)
{
    uint8_t status1,status2;
    bool valid1,valid2;
    int32_t enc1 = roboclaw.ReadEncM1(ADDRESS, &status1, &valid1);
    int32_t enc2 = roboclaw.ReadEncM2(ADDRESS, &status2, &valid2);

    Serial.print("Encoder 1:");
    if (valid1) //Print encoder 1 reading if valid
    {
        Serial.print(enc1);
        Serial.print(" ");
        Serial.print(status1);
        Serial.print(" ");
    }
    else
    {
        Serial.print("invalid ");
    }

    Serial.print("Encoder2:");
    if (valid2) //Print encoder 2 reading if valid
    {
        Serial.print(enc2);
        Serial.print(" ");
        Serial.println(status2);
    }
    else
    {
        Serial.println("invalid ");
    }
}
