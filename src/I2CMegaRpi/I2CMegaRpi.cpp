#include <Arduino.h>
#include <Wire.h>
int a[32];

void dataReceive(int);
void requestEvent();

void setup() {
  // put your setup code here, to run once:
Wire.begin(0x10);
Serial.begin(9600);
Wire.onReceive(dataReceive);
Wire.onRequest(requestEvent);
}

void loop (){
}

void dataReceive(int numBytes) {
  //a[0] = address
  //a[1] = byte 1 of position X int
  //a[2] = byte 2 of position X int
  //a[3] = byte 1 of position Y int
  //a[4] = byte 2 of position Y int
  for(int i = 0; i < numBytes; i++){
    a[i] = Wire.read();
  }
  a[1] = a[1] << 8;
  int posX = a[1] + a[2];
  a[3] = a[3] << 8;
  int posY = a[3] + a[4];
  Serial.print(posX); Serial.print(", "); Serial.println(posY);
}

void requestEvent(void){
 bool oui = 0;
  if(1)
	{
			Wire.write(0);//"Not done Moving"
	}
	else{
		Wire.write(1);//"Done Moving"

	}

}
