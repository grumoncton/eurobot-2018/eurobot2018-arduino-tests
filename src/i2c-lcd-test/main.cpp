#include <Arduino.h>
#include <LiquidCrystal_PCF8574.h>
;

#define I2C_ADDR 0x27
LiquidCrystal_PCF8574 lcd(I2C_ADDR);

void setup() {
  lcd.begin(16, 2);
  lcd.setBacklight(255);
}

void loop() {
  lcd.home();
  lcd.print(millis());
}

