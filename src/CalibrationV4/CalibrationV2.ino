#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void Distance(double distance);
void Rotate(double rotations);
void UpdateExtEncoder();

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial2, 10000);
#define ADDRESS 0x80 //Packet Serial Mode address byte (Mode 7 on RoboClaw)

/*  Combinations that worked good for 5 rotations
    WHEEL_INT_DIMENSION_ERROR 1.00602
    WHEEL_DIA_INT_L 50.044
    WHEEL_SEP_INT 164.307
    WHEEL_EXT_DIMENSION_ERROR 1
    WHEEL_DIA_EXT_L 50.0046
    WHEEL_SEP_EXT 113.4892
*/
//Primary Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
#define WHEEL_INT_DIMENSION_ERROR 1.006 // 1.006 pretty darn beautiful for straight movement
#define WHEEL_DIA_INT_L 50.044 //Internal encoder wheel diameter (mm)
const double WHEEL_DIA_INT_R = WHEEL_DIA_INT_L * WHEEL_INT_DIMENSION_ERROR; //Internal encoder wheel diameter (mm)
#define WHEEL_SEP_INT 164.307 //Distance between internal encoder centers (mm) - 164.307 WORKS GOOD FOR 5 ROTATIONS

#define CPR_EXT 1600 //External encoder ticks per rotation
#define WHEEL_EXT_DIMENSION_ERROR 1
#define WHEEL_DIA_EXT_L 50.0046 //External encoder wheel diameter (mm)
const double WHEEL_DIA_EXT_R = WHEEL_DIA_EXT_L * WHEEL_EXT_DIMENSION_ERROR; //External encoder wheel diameter (mm)
#define WHEEL_SEP_EXT 113.4892 //Distance between external encoder centers (mm) - WORKS GOOD FOR 5 ROTATIONS

double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT_L = (WHEEL_DIA_EXT_L*PI)/CPR_EXT; //External encoder millimeters per count
double MM_PER_CNT_EXT_R = (WHEEL_DIA_EXT_R*PI)/CPR_EXT; //External encoder millimeters per count

double IntDistL;
double IntDistR;

//Movement quantities speed in mm/s and accel in mm/s^2
#define SPEED 200.0
#define ACCEL 100.0
#define DECEL 100.0

//Movement quantities converted speed in counts/s and accel in counts/s^2
double SpeedL = SPEED/MM_PER_CNT_INT_L;
double SpeedR = SPEED/MM_PER_CNT_INT_R;
double AccelL = ACCEL/MM_PER_CNT_INT_L;
double AccelR = ACCEL/MM_PER_CNT_INT_R;
double DecelL = DECEL/MM_PER_CNT_INT_L;
double DecelR = DECEL/MM_PER_CNT_INT_R;

//External Encoder Parameters
int EncoderPinL1 = 2; //Left encoder's green wire
int EncoderPinL2 = 3; //Left encoder's white wire
int EncoderPinR1 = 18; //Right encoder's white wire
int EncoderPinR2 = 19; //Right encoder's green wire
volatile int LastEncodedL;
volatile int LastEncodedR;
volatile long EncoderValueL;
volatile long EncoderValueR;
long EncoderValueLRead; //Current reading variable of the left external encoder
long EncoderValueRRead; //Current reading variable of the right external encoder

//If condition within main() only runs once if set to false afterwards
bool State = true;

int UpdateDelay = 100;

void setup()
{
    Serial.begin(38400);
    Serial.println("========================================================");
    Serial.println("                      Initiated");
    Serial.println("========================================================");
    delay(100);
    
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
    delay(100); //Always allow 100ms delay after resetting encoders

    //External Encoder Pin Setup
    pinMode(EncoderPinL1, INPUT);
    pinMode(EncoderPinL2, INPUT);
    pinMode(EncoderPinR1, INPUT);
    pinMode(EncoderPinR2, INPUT);
    digitalWrite(EncoderPinL1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinL2, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR2, HIGH); //Turn pullup resistor on
    attachInterrupt(digitalPinToInterrupt(EncoderPinL1), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinL2), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR1), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR2), UpdateExtEncoder, CHANGE);
    

}

void loop()
{
  Distance(1800);
  Distance(0);
  while(1);
}

void Distance(double distance)
{
    IntDistL = distance/MM_PER_CNT_INT_L;
    IntDistR = distance/MM_PER_CNT_INT_R;

    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, AccelR, SpeedR, DecelR, IntDistR, AccelL, SpeedL, DecelL, IntDistL, 0);

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
    }

    Serial.println("Theoretical distance traveled ");
    Serial.println(distance);

    Serial.println("Left external encoder counts ");
    Serial.println(EncoderValueL);

    Serial.println("Right external encoder counts ");
    Serial.println(EncoderValueR);

    double MeasuredDistance = ((((double)EncoderValueL * PI * WHEEL_DIA_EXT_L + (double)EncoderValueR * PI * WHEEL_DIA_EXT_R) / 2) / CPR_EXT);
    Serial.println("Distance traveled based on external encoder ");
    Serial.println(MeasuredDistance);
}

void Rotate(double rotations)
{
    IntDistL = (PI * WHEEL_SEP_INT * rotations) / MM_PER_CNT_INT_L;
    IntDistR = (PI * WHEEL_SEP_INT * rotations) / MM_PER_CNT_INT_R;

    double measuredExtRotations;
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, AccelR, SpeedR, DecelR, 0, AccelL, SpeedL, DecelL, 0, 0);
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, AccelR, SpeedR, DecelR, IntDistR, AccelL, SpeedL, DecelL, -IntDistL, 0);

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
    }

    delay(100);

    measuredExtRotations = (((abs(EncoderValueL) * MM_PER_CNT_EXT_L + abs(EncoderValueR) * MM_PER_CNT_EXT_R) / 2)) / (PI * WHEEL_SEP_EXT);

    Serial.println("Theoretical rotations accomplished ");
    Serial.println(rotations);

    Serial.println("Rotations based on external encoders ");
    Serial.println(measuredExtRotations, 4);
}

void UpdateExtEncoder()
{
    int msb_L = digitalRead(EncoderPinL1); //msb_L = most significant bit left motor
    int lsb_L = digitalRead(EncoderPinL2); //lsb_L = least significant bit left motor
    int msb_R = digitalRead(EncoderPinR1); //msb_R = most significant bit right motor
    int lsb_R = digitalRead(EncoderPinR2); //lsb_R = least significant bit right motor

    int encodedL = (msb_L << 1) |lsb_L; //converting the 2 pin of the left motor value to single number
    int encodedR = (msb_R << 1) |lsb_R; //converting the 2 pin of the right motor value to single number
    int sumML  = (LastEncodedL << 2) | encodedL; //adding it to the previous encoded value for left motor
    int sumMR  = (LastEncodedR << 2) | encodedR; //adding it to the previous encoded value for right motor

    if(sumML == 0b1101 || sumML == 0b0100 || sumML == 0b0010 || sumML == 0b1011) EncoderValueL --; //Left encoder forward motion
    if(sumML == 0b1110 || sumML == 0b0111 || sumML == 0b0001 || sumML == 0b1000) EncoderValueL ++; //Left encoder backwards motion
    if(sumMR == 0b1101 || sumMR == 0b0100 || sumMR == 0b0010 || sumMR == 0b1011) EncoderValueR ++; //Right encoder forward motion
    if(sumMR == 0b1110 || sumMR == 0b0111 || sumMR == 0b0001 || sumMR == 0b1000) EncoderValueR --; //Right encoder Backwards motion

    //Save encoder values for next update
    LastEncodedL = encodedL;
    LastEncodedR = encodedR;
}
