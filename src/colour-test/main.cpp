#include <Arduino.h>
#include <Adafruit_TCS34725.h>
#include "ColourSensor.h"
#include "ColourUtils.h"
;

ColourSensor colourSensor;
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);

void setup() {
  Serial.begin(9600);
  colourSensor._tcs = tcs;
  colourSensor._tcs.begin();
  for (uint8_t i = 0; i < 5; i++) {
    pinMode(colourSensorVinPins[i], OUTPUT);
  }
}

void loop() {
  Serial.println(readColourSensorInStack(colourSensor, 0));
  Serial.println(readColourSensorInStack(colourSensor, 1));
}

