#include <Wire.h>
#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>
#include <SimpleTimer.h>
#include <Encoder_Buffer.h>
#include <SPI.h>

//Function decleration
void MoveTo(double x, double y, double a, bool dir, bool adjust);
void AdjustmentTurn(double deltaA);
void UpdateExtPos();
// void UpdateExtEncoder();
void ReadIntEnc();

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial2, 10000);

//Secondary Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)

#define WHEEL_INT_DIMENSION_ERROR 1.00602  // 1.006 pretty darn beautiful for straight movement
#define WHEEL_DIA_INT_L 50.044 //Internal encoder wheel diameter (mm)
const double WHEEL_DIA_INT_R = WHEEL_DIA_INT_L * WHEEL_INT_DIMENSION_ERROR; //Internal encoder wheel diameter (mm)

#define WHEEL_SEP_INT 164.25 //Distance between internal encoder centers (mm)
#define CPR_EXT 1600 //External encoder ticks per rotation

#define WHEEL_EXT_DIMENSION_ERROR 1
#define WHEEL_DIA_EXT_L 49.9237 //External encoder wheel diameter (mm)
const double WHEEL_DIA_EXT_R = WHEEL_DIA_EXT_L * WHEEL_EXT_DIMENSION_ERROR; //External encoder wheel diameter (mm)

#define WHEEL_SEP_EXT 113.4892 //Distance between external encoder centers (mm)
#define ADDRESS 0x80 //Packet Serial Mode address byte (Mode 7 on RoboClaw)
double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT_L = (WHEEL_DIA_EXT_L*PI)/CPR_EXT; //External encoder millimeters per count
double MM_PER_CNT_EXT_R = (WHEEL_DIA_EXT_R*PI)/CPR_EXT; //External encoder millimeters per count

//Movement quantities speed in mm/s and accel in mm/s^2
const double SPEED_ERROR = 1 / WHEEL_INT_DIMENSION_ERROR;
#define DISTSPEED_L 400.0
const double DISTSPEED_R = DISTSPEED_L * SPEED_ERROR;
#define DISTACCEL 200.0
#define DISTDECEL 200.0
#define ROTATESPEED_L 200.0
#define ROTATESPEED_R 200.0
#define ROTATEACCEL 100.0
#define ROTATEDECEL 100.0

//Starting positions in mmm and degrees
#define X_START 0
#define Y_START 0
#define A_START 0
//Robot odometry variables
double XPos = X_START;
double APos = A_START;
double YPos = Y_START;
double APosRad = PI*(APos/180); //Convert angle from degrees to radians in order to be valid with math.h library functions

//External encoders odometry variables
double XPosExt = XPos;
double YPosExt = YPos;
double APosExt = APosRad;
double APosExtRel = APosExt;

//Movement quantities converted speed in counts/s and accel in counts/s^2
double DistSpeedL = DISTSPEED_L/MM_PER_CNT_INT_L;
double DistSpeedR = DISTSPEED_R/MM_PER_CNT_INT_R;

double DistAccel = DISTACCEL/MM_PER_CNT_INT_L;
double DistDecel = DISTDECEL/MM_PER_CNT_INT_L;

double RotateSpeedL = ROTATESPEED_L/MM_PER_CNT_INT_L;
double RotateSpeedR = ROTATESPEED_R/MM_PER_CNT_INT_R;

double RotateAccel = ROTATEACCEL/MM_PER_CNT_INT_L;
double RotateDecel = ROTATEDECEL/MM_PER_CNT_INT_L;


//Game variable where 0 is normal and 1 is mirrored along the x axis
bool Game = 0;

bool FirstAdjust = false;
bool SecondAdjust = false;

//External Encoder Parameters
#define EncoderCSR 2
#define EncoderCSL 3
long EncoderValueR = 0;
long EncoderValueL = 0;
Encoder_Buffer EncoderR(EncoderCSR);
Encoder_Buffer EncoderL(EncoderCSL);
// SimpleTimer timer;
// uint8_t EncoderPinL1 = 2; //Left encoder's green wire
// uint8_t EncoderPinL2 = 3; //Left encoder's white wire
// uint8_t EncoderPinR1 = 18; //Right encoder's white wire
// uint8_t EncoderPinR2 = 19; //Right encoder's green wire
// volatile uint8_t LastEncodedL; // I don't think I need this with Counter Click
// volatile uint8_t LastEncodedR; // I don't think I need this with Counter Click

//If condition within main() only runs once if set to false afterwards
bool State = true;

bool Collision = false;
bool PrecisionCorrection = false;

double AMaxError = (3/360)*2*PI;
uint8_t XMaxError = 2;
uint8_t YMaxError = 2;

uint8_t UpdateCnt; // no need for Volatile
uint8_t MaxPulseDist = 20; //Update position during distance command after this many counts
uint8_t MaxPulseRotate = 8; //Update position during rotation command after this many counts
uint8_t ReadBufferDelay = 200; //Read RoboClaw every X milliseconds

#define DistanceArrayLength 5 //Running average length during distance command
#define RotateArrayLength 2 //Running average length during rotation command
bool DistanceBool = false;

uint8_t MaxJerk = 80;

void setup()
{
    //Set baud rate to be communicated with RoboClaw (Must be set in RoboClaw parameters)
    SPI.begin();
    EncoderR.initEncoder();
    EncoderL.initEncoder();
    // timer.setInterval(1, UpdateExtEncoder);
    Serial3.begin(38400);
    Serial.begin(38400);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
    delay(100); //Always allow 100ms delay after resetting encoders
    Serial.println("Current Theoretical Position: 0, 0, 0!");
    Serial.println("================================================================================================!");
    Serial.println("Initialized!");
    Serial.println("================================================================================================!");

    //Accelerometer pin
    // Wire.begin();
    // pinMode(A0, INPUT);

    //External Encoder Pin Setup
    // no need for interrupts with Counter Clicks
}

void loop()
{
  //Move under water tower to catch 8 balls
  //Activate ball launcher
  MoveTo(1000,0,-1,0,0);
  // MoveTo(0,0,-1,0,0);
  // MoveTo(0,0,-1,1,0);
  while(1);
}

//x and y are destination coordinates
//a is the robot's orientation to be adjusted to once the coordinates are reached (-1 for no adjustment angle)
//start decides which motors to be engaged during first AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//end decides which motors to be engaged during second AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//dir set to 0 for forward movement, 1 for backwards movement
//adjust set to 0 to skip final adjustment
void MoveTo(double x, double y, double a, bool dir, bool adjust)
{
    do
    {
        //Movement variables
        double xDist; //X Distance in mm to be executed during MoveTo command
        double xDistCountIntL; //X Distance in counts to be executed during MoveTo command
        double xDistCountIntR; //X Distance in counts to be executed during MoveTo command
        double yDist; //Y Distance in mm to be executed during MoveTo command
        double yDistCountIntL; //Y Distance in counts to be executed during MoveTo command
        double yDistCountIntR; //Y Distance in counts to be executed during MoveTo command
        double travelDistL; //Distance to be accomplished during MoveTo in counts
        double travelDistR; //Distance to be accomplished during MoveTo in counts
        double adjustedA; //Angle to be oriented to in radians
        double deltaA; //Angle between current position and adjustedA in radians

        double aError;
        double xError;
        double yError;

        double adjustmentDir = false; //Set dir to 1 temporarily if adjustment needs to move in reverse, dir will be reset to 0 at end of loop

        double aRad = PI*(a/180);

        PrecisionCorrection = false;
        Collision = false;

        xDist = x-XPos;
        xDistCountIntL = xDist/MM_PER_CNT_INT_L;
        xDistCountIntR = xDist/MM_PER_CNT_INT_R;

        yDist = y-YPos;
        yDistCountIntL = yDist/MM_PER_CNT_INT_L;
        yDistCountIntR = yDist/MM_PER_CNT_INT_R;

        //Adjustment angle calculations with resulting angle between 0 and 360 degrees (valid for both games)
        if (((xDist > 0 && yDist >= 0) && (dir == 0)) || ((xDist < 0 && yDist <= 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(yDist), fabs(xDist));
        }
        else if (((xDist <= 0 && yDist > 0) && (dir == 0)) || ((xDist >= 0 && yDist < 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
        }
        else if (((xDist < 0 && yDist <= 0) && (dir == 0)) || ((xDist > 0 && yDist >= 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
        }
        else if (((xDist >= 0 && yDist < 0) && (dir == 0)) || ((xDist <= 0 && yDist > 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
        }
        else if ((xDist == 0) && (yDist == 0))
        {
            adjustedA = APosRad;
        }

        deltaA = adjustedA-APosRad;

        if ((adjust) && (abs(xDist) < 100) && (abs(yDist) < 100) && ((deltaA > PI/2) || (deltaA < -PI/2)))
            {
                dir = 1;
                adjustmentDir = true;

                if (((xDist > 0 && yDist >= 0) && (dir == 0)) || ((xDist < 0 && yDist <= 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(yDist), fabs(xDist));
                }
                else if (((xDist <= 0 && yDist > 0) && (dir == 0)) || ((xDist >= 0 && yDist < 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
                }
                else if (((xDist < 0 && yDist <= 0) && (dir == 0)) || ((xDist > 0 && yDist >= 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
                }
                else if (((xDist >= 0 && yDist < 0) && (dir == 0)) || ((xDist <= 0 && yDist > 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
                }
                else if ((xDist == 0) && (yDist == 0))
                {
                    adjustedA = APosRad;
                }

                deltaA = adjustedA-APosRad;
            }

        if (deltaA != 0)
        {
            AdjustmentTurn(deltaA); //First adjustment turn to align robot with destination coordinate
            APosRad = APosExtRel;
            XPos = XPosExt;
            YPos = YPosExt;

            roboclaw.ResetEncoders(ADDRESS);
            delay(100);
        }

        travelDistL = hypot(xDistCountIntL, yDistCountIntL);
        travelDistR = hypot(xDistCountIntR, yDistCountIntR);

        if (travelDistL != 0 || travelDistR != 0 && !Collision)
        {
            if (dir == 0) //Forward movement
            {
                roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, DistAccel, DistSpeedR, DistDecel, travelDistR, DistAccel, DistSpeedL, DistDecel, travelDistL, 0);
            }
            else //Backwards movement
            {
                roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, DistAccel, DistSpeedR, DistDecel, -travelDistR, DistAccel, DistSpeedL, DistDecel, -travelDistL, 0);
            }

            DistanceBool = true;
            UpdateExtPos();
            DistanceBool = false;

            roboclaw.ResetEncoders(ADDRESS);
            delay(100);

            XPos=XPosExt;
            YPos=YPosExt;
            APosRad = APosExtRel;

        }
        if (a == -1) //No adjustment angle (same as arriving orientation)
        {
            deltaA = 0;
            a = 180*(adjustedA/PI);
        }
        else
        {
            deltaA = aRad-APosRad;
        }

        if (deltaA != 0 && !Collision)
        {
            AdjustmentTurn(deltaA); //Second adjustment turn to align robot with final angle argument in MoveTo()
            XPos = XPosExt;
            YPos = YPosExt;
            APosRad = APosExtRel;

            roboclaw.ResetEncoders(ADDRESS);
            delay(100);
        }

        if (adjustmentDir)
        {
            dir = 0;
        }

        if (adjust)
        {
            aError = APosRad - (a/180)*PI;
            if (aError > PI)
            {
                aError -= 2*PI;
            }
            else if (aError < -PI)
            {
                aError += 2*PI;
            }
            if (abs(aError) > AMaxError)
            {
                PrecisionCorrection = true;
                Serial.print("Rotation precision error ");
                Serial.print(aError);
                Serial.print("!");
            }

            xError = XPos-x;
            yError = YPos-y;
            if (abs(xError) > XMaxError)
            {
                PrecisionCorrection = true;
                Serial.print("Distance precision X error ");
                Serial.print(xError);
                Serial.print("!");
            }
            if (yError > YMaxError)
            {
                PrecisionCorrection = true;
                Serial.print("Distance precision y error ");
                Serial.print(yError);
                Serial.print("!");
            }
        }

        Serial.print("Current Theoretical Position: X ");
        Serial.print(x);
        Serial.print(", Y ");
        Serial.print(y);
        Serial.print(", A ");
        Serial.println(a);

        Serial.println("Current Position Based on ExtEnc: X ");
        Serial.print(XPosExt);
        Serial.print(", Y ");
        Serial.print(YPosExt);
        Serial.print(", ARel ");
        Serial.println((APosExtRel/PI)*180);

        Serial.println("------------------------------------------------------------------------------------------------!");
        Serial.println("Position reached!");
        Serial.println("------------------------------------------------------------------------------------------------!");
    }
    while (PrecisionCorrection);
}

void AdjustmentTurn(double deltaA)
{
    double turnDistL; //Distance to be accomplished by each wheel while turning
    double turnDistR; //Distance to be accomplished by each wheel while turning

    //Conditions to determine smallest angle to be accomplished (between -PI and PI)
    if (deltaA > PI)
    {
        deltaA -= 2*PI;
    }
    else if (deltaA < -PI)
    {
        deltaA += 2*PI;
    }

    turnDistL = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT_L;
    turnDistR = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT_R;

    //Turn achieved by position command with distance arguments having opposite signs to each other
    if (((deltaA >= 0) && (!Game)) || ((deltaA < 0) && (Game)))//Positive rotation based on x axis (CCW)
    {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccel, RotateSpeedR, RotateDecel, turnDistR, RotateAccel, RotateSpeedL, RotateDecel, -turnDistL, 0);
    }
    else //Negative rotation based on x axis (CW)
    {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccel, RotateSpeedR, RotateDecel, -turnDistR, RotateAccel, RotateSpeedL, RotateDecel, turnDistL, 0);
    }

    UpdateExtPos();
}

void UpdateExtPos()
{
    //Initiate and reset encoder variables
    double leftDist = 0; //Distance traveled by left motor since last updated
    double rightDist = 0; //Distance traveled by right motor since last updated
    double centerDist = 0; //Distance traveled by the center of the robot since last updated
    double arcA = 0; //Arc accomplished by the robot since last updated where the angle is based on the axle of the robot at starting angle
    long encoderValueLRead = 0; //Current reading variable of the left external encoder
    long encoderValueRRead = 0; //Current reading variable of the right external encoder
    long LastEncoderValueLRead = 0;
    long LastEncoderValueRRead = 0;
    EncoderL.clearEncoderCount();
    EncoderR.clearEncoderCount();
    // EncoderValueL = 0; //Reset global EncoderValueL attached to interrupt
    // EncoderValueR = 0; //Reset global EncoderValueR attached to interrupt

    //Distance running average variables
    double arcTotal = 0;
    double arcs[DistanceArrayLength] = {0};
    int index = 0;
    double arcAverage = 0;

    //Rotate running average variables
    double xDisp = 0;
    double yDisp = 0;
    double xDispTotal = 0;
    double yDispTotal = 0;
    double xDispArr[RotateArrayLength] = {0};
    double yDispArr[RotateArrayLength] = {0};
    double xDispAverage = 0;
    double yDispAverage = 0;

    //Acceleration variables
    int accel = 0;
    int lastAccel = 336;
    int jerk = 0;

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {

        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);

        //accel = analogRead(A0);
        //jerk = abs(accel-lastAccel);
        // if (jerk > MaxJerk)
        // {
        //     roboclaw.DutyM1M2(ADDRESS, 0, 0);
        //     Collision = true;
        //     Serial.print("Collision detected, jerk measured ");
        //     Serial.print(jerk);
        //     delay(5000);
        //     break;
        // }
        // lastAccel = accel;

        uint32_t start = millis();
        uint32_t end = millis();
        while ((end - start) < ReadBufferDelay)
        {
            encoderValueLRead = EncoderL.readEncoder(); //Store left encoder reading as a variable
            encoderValueRRead = EncoderR.readEncoder(); //Store right encoder reading as a
            if (  fabs(encoderValueLRead-LastEncoderValueLRead) >= MaxPulseDist || fabs(encoderValueRRead-LastEncoderValueRRead) >= MaxPulseRotate  )
            {
                // encoderValueLRead = EncoderL.readEncoder(); //Store left encoder reading as a variable
                // encoderValueRRead = EncoderR.readEncoder(); //Store right encoder reading as a variable
                leftDist = (encoderValueLRead-LastEncoderValueLRead)*MM_PER_CNT_EXT_L;
                rightDist = (encoderValueRRead-LastEncoderValueRRead)*MM_PER_CNT_EXT_R;
                centerDist = ((leftDist+rightDist)/2);
                if (Game)
                {
                    arcA = (leftDist-rightDist)/WHEEL_SEP_EXT;
                }
                else
                {
                    arcA = (rightDist-leftDist)/WHEEL_SEP_EXT;
                }

                if (DistanceBool)
                {
                    arcTotal -= arcs[index];
                    arcs[index] = arcA;
                    arcTotal += arcs[index];
                    index++;
                    if (index >= DistanceArrayLength)
                    {
                        index = 0;
                    }
                    arcAverage = arcTotal/DistanceArrayLength;
                    if (fabs(arcA) > fabs(arcAverage))
                    {
                        arcA = 0;
                    }
                }

                if (arcA != 0)
                {
                    xDisp = (centerDist/arcA)*(sin(APosExt + arcA)-sin(APosExt));
                    yDisp = (centerDist/arcA)*(cos(APosExt)-cos(APosExt+arcA));
                }
                else
                {
                    xDisp = centerDist*cos(APosExt); //Approximation valid  for a small arcA
                    yDisp = centerDist*sin(APosExt); //Approximation valid  for a small arcA
                }

                if (!DistanceBool)
                {
                    xDispTotal -= xDispArr[index];
                    yDispTotal -= yDispArr[index];
                    xDispArr[index] = xDisp;
                    yDispArr[index] = yDisp;
                    xDispTotal += xDispArr[index];
                    yDispTotal += yDispArr[index];
                    index++;
                    if (index >= RotateArrayLength)
                    {
                        index = 0;
                    }
                    xDispAverage = xDispTotal/RotateArrayLength;
                    yDispAverage = yDispTotal/RotateArrayLength;
                    if (fabs(xDisp) > fabs(xDispAverage))
                    {
                        xDisp = 0;
                    }
                    if (fabs(yDisp) > fabs(yDispAverage))
                    {
                        yDisp = 0;
                    }
                }

                XPosExt += xDisp;
                YPosExt += yDisp;

                APosExt += arcA;

                //Conditions to determine smallest angle
                if (APosExt > PI)
                {
                    APosExt = -2*PI+APosExt;
                }
                else if (APosExt < -PI)
                {
                    APosExt = 2*PI+APosExt;
                }

                //Convert angle relative to axle to the global coordinates
                if (APosExt < 0)
                {
                    APosExtRel = 2*PI+APosExt;
                }
                else
                {
                    APosExtRel = APosExt;
                }

                //Save encoder values for next update
                LastEncoderValueLRead = encoderValueLRead;
                LastEncoderValueRRead = encoderValueRRead;

                UpdateCnt = 0;
            }
            end = millis();
        }
    }
}
