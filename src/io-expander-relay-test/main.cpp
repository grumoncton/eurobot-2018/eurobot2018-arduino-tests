#include <Arduino.h>
#include <Wire.h>

#define ADDRESS 0x20

void setup() {
  Wire.begin();
  Serial.begin(9600);
}

void write(uint8_t state) {
  Wire.beginTransmission(ADDRESS);
  Wire.write(state);
  Wire.endTransmission();
  Serial.println(state);
  delay(1000);
}

// On veux faire un compteur binaire pour 3 relays alors on compte de 0 a 2^3 avant de recommencer
void loop() {
  for (int i = 0; i < pow(2, 3); i++) {
    write(i);
  }
}

