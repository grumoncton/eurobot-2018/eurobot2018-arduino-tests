#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void Distance(double distance);
void Rotate(double rotations);
void UpdateExtEncoderL();
void UpdateExtEncoderR();

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial2, 10000);
#define ADDRESS 0x80 //Packet Serial Mode address byte (Mode 7 on RoboClaw)

//Setting up communication with XBee
#define XBee Serial3

//Primary Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
double WHEEL_DIA_INT_R = 50.32; //Internal right encoder wheel diameter (mm) Reduce if undershoot
double WHEEL_DIA_INT_L = 0.994*WHEEL_DIA_INT_R; //Internal left encoder wheel diameter (mm) 
#define WHEEL_SEP_INT 163.56 //Distance between internal encoder centers (mm) Increase if undershoot
#define CPR_EXT 2500 //External encoder ticks per rotation
#define WHEEL_DIA_EXT 63.66 //External encoder wheel diameter (mm) Increase if undershoot
#define WHEEL_SEP_EXT 110.2 //Distance between external encoder centers (mm) Decrease if undershoot
double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count

//Secondary Robot Dimensions
// #define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
// double WHEEL_DIA_INT_R = 50.19; //Internal right encoder wheel diameter (mm)
// double WHEEL_DIA_INT_L = 0.9972*WHEEL_DIA_INT_R; //Internal left encoder wheel diameter (mm)
// #define WHEEL_SEP_INT 190.57 //Distance between internal encoder centers (mm)
// #define CPR_EXT 2500 //External encoder ticks per rotation
// #define WHEEL_DIA_EXT 63.48 //External encoder wheel diameter (mm)
// #define WHEEL_SEP_EXT 115.3 //Distance between external encoder centers (mm) Decrease if undershoot
// double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
// double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
// double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count

double IntDistL;
double IntDistR;

//Movement quantities speed in mm/s and accel in mm/s^2
#define SPEED 300.0
#define ACCEL 300.0
#define DECEL 300.0

//Movement quantities converted speed in counts/s and accel in counts/s^2
double SpeedL = SPEED/MM_PER_CNT_INT_L;
double AccelL = ACCEL/MM_PER_CNT_INT_L;
double DecelL = DECEL/MM_PER_CNT_INT_L;
double SpeedR = SPEED/MM_PER_CNT_INT_R;
double AccelR = ACCEL/MM_PER_CNT_INT_R;
double DecelR = DECEL/MM_PER_CNT_INT_R;

//External Encoder Parameters
int EncoderPinL1 = 2; //Left encoder's green wire
int EncoderPinL2 = 3; //Left encoder's white wire
int EncoderPinR1 = 18; //Right encoder's white wire
int EncoderPinR2 = 19; //Right encoder's green wire
volatile int LastEncodedL;
volatile int LastEncodedR;
volatile long EncoderValueL;
volatile long EncoderValueR;
long EncoderValueLRead; //Current reading variable of the left external encoder
long EncoderValueRRead; //Current reading variable of the right external encoder

//If condition within main() only runs once if set to false afterwards
bool State = true;

int UpdateDelay = 100;

void setup()
{
    //Set baud rate to be communicated with RoboClaw (Must be set in RoboClaw parameters)
    XBee.begin(38400);
    Serial.begin(38400);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
    delay(100); //Always allow 100ms delay after resetting encoders

    //Accelerometer pin
    pinMode(A0, INPUT);

    //External Encoder Pin Setup
    pinMode(EncoderPinL1, INPUT);
    pinMode(EncoderPinL2, INPUT);
    pinMode(EncoderPinR1, INPUT);
    pinMode(EncoderPinR2, INPUT);
    digitalWrite(EncoderPinL1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinL2, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR2, HIGH); //Turn pullup resistor on
    attachInterrupt(digitalPinToInterrupt(EncoderPinL1), UpdateExtEncoderL, RISING);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR1), UpdateExtEncoderR, RISING);
}

void loop()
{
    if (State)
    {
        //Rotate(rotations in full turns);
        Rotate(3);
        State = false;
    }
    Serial.print(EncoderValueL);
    Serial.print("\t");
    Serial.println(EncoderValueR);
}

void Distance(double distance)
{
    IntDistL = distance/MM_PER_CNT_INT_L;
    IntDistR = distance/MM_PER_CNT_INT_R;

    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, AccelR, SpeedR, DecelR, IntDistR, AccelL, SpeedL, DecelL, IntDistL, 0);

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
        // XBee.print(EncoderValueL);
        // XBee.print("\t");
        // XBee.print(EncoderValueR);
        // XBee.print("!");
    }

    XBee.print("Theoretical distance traveled ");
    XBee.print(distance);
    XBee.print("!");

    XBee.print("Left external encoder counts ");
    XBee.print(EncoderValueL);
    XBee.print("!");

    XBee.print("Right external encoder counts ");
    XBee.print(EncoderValueR);
    XBee.print("!");

    double MeasuredDistance = ((((double)EncoderValueL + (double)EncoderValueR) / 2) / CPR_EXT) * PI * WHEEL_DIA_EXT;
    XBee.print("Distance traveled based on external encoder ");
    XBee.print(MeasuredDistance);
    XBee.print("!");
}

void Rotate(double rotations)
{
    IntDistL = (PI * WHEEL_SEP_INT * rotations) / MM_PER_CNT_INT_L;
    IntDistR = (PI * WHEEL_SEP_INT * rotations) / MM_PER_CNT_INT_R;
    double measuredExtRotations;
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, AccelR, SpeedR, DecelR, 0, AccelL, SpeedL, DecelL, 0, 0);
    roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, AccelR, SpeedR, DecelR, IntDistR, AccelL, SpeedL, DecelL, -IntDistL, 0);

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(UpdateDelay); //Delay of at least 25ms so that UpdateExtPos() interrupts don't affect buffer interrupts
    }

    delay(100);

    measuredExtRotations = (((abs(EncoderValueL) + abs(EncoderValueR)) / 2) * MM_PER_CNT_EXT) / (PI * WHEEL_SEP_EXT);

    XBee.print("Theoretical rotations accomplished ");
    XBee.print(rotations);
    XBee.print("!");

    XBee.print("Rotations based on external encoders ");
    XBee.print(measuredExtRotations, 4);
    XBee.print("!");
}

void UpdateExtEncoderL()
{
    if (digitalRead(EncoderPinL2) == LOW) {EncoderValueL++;}
    else{EncoderValueL--;}
}

void UpdateExtEncoderR()
{
    if (digitalRead(EncoderPinR2) == LOW) {EncoderValueR++;}
    else{EncoderValueR--;}
}