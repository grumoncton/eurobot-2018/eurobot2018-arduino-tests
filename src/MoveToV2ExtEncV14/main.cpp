#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void MoveTo(double x, double y, double a, bool dir, bool adjust);
void AdjustmentTurn(double deltaA);
void UpdateExtPos();
void UpdateExtEncoderL();
void UpdateExtEncoderR();
void ReadIntEnc();

//Setting up communication with RoboClaw on Serial3 with 10ms timeout
RoboClaw roboclaw(&Serial2, 10000);

//Setting up communication with XBee
#define XBee Serial3
#define ADDRESS 0x80 //Packet Serial Mode address byte (Mode 7 on RoboClaw)

//Prototype Robot Dimensions 
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
double WHEEL_DIA_INT_R = 50.32; //Internal right encoder wheel diameter (mm) 
double WHEEL_DIA_INT_L = 0.994*WHEEL_DIA_INT_R; //Internal left encoder wheel diameter (mm) 
#define WHEEL_SEP_INT 163.56 //Distance between internal encoder centers (mm) 
#define CPR_EXT 2500 //External encoder ticks per rotation
#define WHEEL_DIA_EXT 63.66 //External encoder wheel diameter (mm) 
#define WHEEL_SEP_EXT 110.2 //Distance between external encoder centers (mm) 
double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count

//Secondary Robot Dimensions
// #define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
// double WHEEL_DIA_INT_R = 50.19; //Internal right encoder wheel diameter (mm)
// double WHEEL_DIA_INT_L = 0.9972*WHEEL_DIA_INT_R; //Internal left encoder wheel diameter (mm)
// #define WHEEL_SEP_INT 190.57 //Distance between internal encoder centers (mm)
// #define CPR_EXT 2500 //External encoder ticks per rotation
// #define WHEEL_DIA_EXT 63.48 //External encoder wheel diameter (mm)
// #define WHEEL_SEP_EXT 115.3 //Distance between external encoder centers (mm) Decrease if undershoot
// double MM_PER_CNT_INT_L = (WHEEL_DIA_INT_L*PI)/CPR_INT; //Internal encoder millimeters per count
// double MM_PER_CNT_INT_R = (WHEEL_DIA_INT_R*PI)/CPR_INT; //Internal encoder millimeters per count
// double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT; //External encoder millimeters per count

//Movement quantities speed in mm/s and accel in mm/s^2
#define DISTSPEED 600.0
#define DISTACCEL 400.0
#define DISTDECEL 400.0
#define ROTATESPEED 300.0
#define ROTATEACCEL 300.0
#define ROTATEDECEL 300.0

//Starting positions in mmm and degrees
#define X_START 0
#define Y_START 0
#define A_START 0

//Robot odometry variables
double XPos = X_START;
double APos = A_START;
double YPos = Y_START;
double APosRad = PI*(APos/180); //Convert angle from degrees to radians in order to be valid with math.h library functions

//External encoders odometry variables
double XPosExt = XPos;
double YPosExt = YPos;
double APosExt = APosRad;
double APosExtRel = APosExt;

//Movement quantities converted speed in counts/s and accel in counts/s^2
double DistSpeedL = DISTSPEED/MM_PER_CNT_INT_L;
double DistAccelL = DISTACCEL/MM_PER_CNT_INT_L;
double DistDecelL = DISTDECEL/MM_PER_CNT_INT_L;
double RotateSpeedL = ROTATESPEED/MM_PER_CNT_INT_L;
double RotateAccelL = ROTATEACCEL/MM_PER_CNT_INT_L;
double RotateDecelL = ROTATEDECEL/MM_PER_CNT_INT_L;
double DistSpeedR = DISTSPEED/MM_PER_CNT_INT_R;
double DistAccelR = DISTACCEL/MM_PER_CNT_INT_R;
double DistDecelR = DISTDECEL/MM_PER_CNT_INT_R;
double RotateSpeedR = ROTATESPEED/MM_PER_CNT_INT_R;
double RotateAccelR = ROTATEACCEL/MM_PER_CNT_INT_R;
double RotateDecelR = ROTATEDECEL/MM_PER_CNT_INT_R;

//Game variable where 0 is normal and 1 is mirrored along the x axis
bool Game = 0;

bool FirstAdjust = false;
bool SecondAdjust = false;

//External Encoder Parameters
uint8_t EncoderPinL1 = 2; //Left encoder's green wire
uint8_t EncoderPinL2 = 3; //Left encoder's white wire
uint8_t EncoderPinR1 = 18; //Right encoder's white wire
uint8_t EncoderPinR2 = 19; //Right encoder's green wire
volatile uint8_t LastEncodedL;
volatile uint8_t LastEncodedR;
volatile long EncoderValueL;
volatile long EncoderValueR;

//If condition within main() only runs once if set to false afterwards
bool State = true;

bool Collision = false;
bool PrecisionCorrection = false;

double AMaxError = (3/360)*2*PI;
uint8_t XMaxError = 2;
uint8_t YMaxError = 2;

volatile uint8_t UpdateCnt;
uint8_t MaxPulseDist = 25; //Update position during distance command after this many counts
uint8_t MaxPulseRotate = 25; //Update position during rotation command after this many counts
uint8_t ReadBufferDelay = 200; //Read RoboClaw every X milliseconds

#define DistanceArrayLength 5 //Running average length during distance command
#define RotateArrayLength 5 //Running average length during rotation command
bool DistanceBool = false;

uint8_t MaxJerk = 80;

uint8_t MotorState = 0b11;

void setup()
{
    //Set baud rate to be communicated with RoboClaw (Must be set in RoboClaw parameters)
    Serial3.begin(38400);
    Serial.begin(38400);
    XBee.begin(38400);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS); //Reset internal encoders to 0
    delay(100); //Always allow 100ms delay after resetting encoders
    XBee.print("Current Theoretical Position: 0, 0, 0!");
    XBee.print("================================================================================================!");
    XBee.print("Initialized!");
    XBee.print("================================================================================================!");

    //Accelerometer pin
    pinMode(A0, INPUT);

    //External Encoder Pin Setup
    pinMode(EncoderPinL1, INPUT);
    pinMode(EncoderPinL2, INPUT);
    pinMode(EncoderPinR1, INPUT);
    pinMode(EncoderPinR2, INPUT);
    digitalWrite(EncoderPinL1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinL2, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR1, HIGH); //Turn pullup resistor on
    digitalWrite(EncoderPinR2, HIGH); //Turn pullup resistor on
    attachInterrupt(digitalPinToInterrupt(EncoderPinL1), UpdateExtEncoderL, RISING);
    attachInterrupt(digitalPinToInterrupt(EncoderPinR1), UpdateExtEncoderR, RISING);
}

void loop()
{
    if (State)
    {
        //MoveTo(X, Y, A, Dir, Adj)
        State = false;
    }
}


//x and y are destination coordinates
//a is the robot's orientation to be adjusted to once the coordinates are reached (-1 for no adjustment angle)
//start decides which motors to be engaged during first AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//end decides which motors to be engaged during second AdjustmentTurn() (1 = Left, 2 = Right, 3 = Simultaneous)
//dir set to 0 for forward movement, 1 for backwards movement
//adjust set to 0 to skip final adjustment
void MoveTo(double x, double y, double a, bool dir, bool adjust)
{
    do
    {
        //Movement variables
        double xDist; //X Distance in mm to be executed during MoveTo command
        double xDistCountIntL; //X Distance in counts to be executed during MoveTo command
        double xDistCountIntR; //X Distance in counts to be executed during MoveTo command
        double yDist; //Y Distance in mm to be executed during MoveTo command
        double yDistCountIntL; //Y Distance in counts to be executed during MoveTo command
        double yDistCountIntR; //Y Distance in counts to be executed during MoveTo command
        double travelDistL; //Distance to be accomplished during MoveTo in counts
        double travelDistR; //Distance to be accomplished during MoveTo in counts
        double adjustedA; //Angle to be oriented to in radians
        double deltaA; //Angle between current position and adjustedA in radians

        double aError;
        double xError;
        double yError;

        double adjustmentDir = false; //Set dir to 1 temporarily if adjustment needs to move in reverse, dir will be reset to 0 at end of loop

        double aRad = PI*(a/180);

        PrecisionCorrection = false;
        Collision = false;

        xDist = x-XPos;
        xDistCountIntL = xDist/MM_PER_CNT_INT_L;
        xDistCountIntR = xDist/MM_PER_CNT_INT_R;
        yDist = y-YPos;
        yDistCountIntL = yDist/MM_PER_CNT_INT_L;
        yDistCountIntR = yDist/MM_PER_CNT_INT_R;

        //Adjustment angle calculations with resulting angle between 0 and 360 degrees (valid for both games)
        if (((xDist > 0 && yDist >= 0) && (dir == 0)) || ((xDist < 0 && yDist <= 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(yDist), fabs(xDist));
        }
        else if (((xDist <= 0 && yDist > 0) && (dir == 0)) || ((xDist >= 0 && yDist < 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
        }
        else if (((xDist < 0 && yDist <= 0) && (dir == 0)) || ((xDist > 0 && yDist >= 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
        }
        else if (((xDist >= 0 && yDist < 0) && (dir == 0)) || ((xDist <= 0 && yDist > 0) && (dir == 1)))
        {
            adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
        }
        else if ((xDist == 0) && (yDist == 0))
        {
            adjustedA = APosRad;
        }

        deltaA = adjustedA-APosRad;

        if ((adjust) && (abs(xDist) < 100) && (abs(yDist) < 100) && ((deltaA > PI/2) || (deltaA < -PI/2)))
            {
                dir = 1;
                adjustmentDir = true;

                if (((xDist > 0 && yDist >= 0) && (dir == 0)) || ((xDist < 0 && yDist <= 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(yDist), fabs(xDist));
                }
                else if (((xDist <= 0 && yDist > 0) && (dir == 0)) || ((xDist >= 0 && yDist < 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(xDist), fabs(yDist))+PI/2;
                }
                else if (((xDist < 0 && yDist <= 0) && (dir == 0)) || ((xDist > 0 && yDist >= 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(yDist), fabs(xDist))+PI;
                }
                else if (((xDist >= 0 && yDist < 0) && (dir == 0)) || ((xDist <= 0 && yDist > 0) && (dir == 1)))
                {
                    adjustedA = atan2(fabs(xDist), fabs(yDist))+(3*PI)/2;
                }
                else if ((xDist == 0) && (yDist == 0))
                {
                    adjustedA = APosRad;
                }

                deltaA = adjustedA-APosRad;
            }

        if (deltaA != 0)
        {
            AdjustmentTurn(deltaA); //First adjustment turn to align robot with destination coordinate
            APosRad = APosExtRel;
            XPos = XPosExt;
            YPos = YPosExt;

            roboclaw.ResetEncoders(ADDRESS);
            delay(100);
        }

        travelDistL = hypot(xDistCountIntL, yDistCountIntL);
        travelDistR = hypot(xDistCountIntR, yDistCountIntR);

        if ((travelDistL != 0 && !Collision) && (travelDistR != 0 && !Collision))
        {
            if (dir == 0) //Forward movement
            {
                roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, DistAccelR, DistSpeedR, DistDecelR, travelDistR, DistAccelL, DistSpeedL, DistDecelL, travelDistL, 0);
            }
            else //Backwards movement
            {
                roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, DistAccelR, DistSpeedR, DistDecelR, -travelDistR, DistAccelL, DistSpeedL, DistDecelL, -travelDistL, 0);
            }

            DistanceBool = true;
            UpdateExtPos();
            DistanceBool = false;

            roboclaw.ResetEncoders(ADDRESS);
            delay(100);

            XPos=XPosExt;
            YPos=YPosExt;
            APosRad = APosExtRel;

        }
        if (a == -1) //No adjustment angle (same as arriving orientation)
        {
            deltaA = 0;
            a = 180*(adjustedA/PI);
        }
        else
        {
            deltaA = aRad-APosRad;
        }

        if (deltaA != 0 && !Collision)
        {
            AdjustmentTurn(deltaA); //Second adjustment turn to align robot with final angle argument in MoveTo()
            XPos = XPosExt;
            YPos = YPosExt;
            APosRad = APosExtRel;

            roboclaw.ResetEncoders(ADDRESS);
            delay(100);
        }

        if (adjustmentDir)
        {
            dir = 0;
        }

        if (adjust)
        {
            aError = APosRad - (a/180)*PI;
            if (aError > PI)
            {
                aError -= 2*PI;
            }
            else if (aError < -PI)
            {
                aError += 2*PI;
            }
            if (fabs(aError) > AMaxError)
            {
                PrecisionCorrection = true;
                XBee.print("Rotation precision error ");
                XBee.print(aError);
                XBee.print("!");
            }

            xError = XPos-x;
            yError = YPos-y;
            if (fabs(xError) > XMaxError)
            {
                PrecisionCorrection = true;
                XBee.print("Distance precision X error ");
                XBee.print(xError);
                XBee.print("!");
            }
            if (yError > YMaxError)
            {
                PrecisionCorrection = true;
                XBee.print("Distance precision y error ");
                XBee.print(yError);
                XBee.print("!");
            }
        }

        XBee.print("Current Theoretical Position: X ");
        XBee.print(x);
        XBee.print(", Y ");
        XBee.print(y);
        XBee.print(", A ");
        XBee.print(a);
        XBee.print('!');
        XBee.print("Current Position Based on ExtEnc: X ");
        XBee.print(XPosExt);
        XBee.print(", Y ");
        XBee.print(YPosExt);
        XBee.print(", ARel ");
        XBee.print((APosExtRel/PI)*180);
        XBee.print('!');
        XBee.print("------------------------------------------------------------------------------------------------!");
        XBee.print("Position reached!");
        XBee.print("------------------------------------------------------------------------------------------------!");
    }
    while (PrecisionCorrection);
}

void AdjustmentTurn(double deltaA)
{
    double turnDistL; //Distance to be accomplished by each wheel while turning
    double turnDistR; //Distance to be accomplished by each wheel while turning

    //Conditions to determine smallest angle to be accomplished (between -PI and PI)
    if (deltaA > PI)
    {
        deltaA -= 2*PI;
    }
    else if (deltaA < -PI)
    {
        deltaA += 2*PI;
    }

    turnDistL = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT_L;
    turnDistR = (PI*WHEEL_SEP_INT*(fabs(deltaA)/(2*PI)))/MM_PER_CNT_INT_R;

    //Turn achieved by position command with distance arguments having opposite signs to each other
    if (((deltaA >= 0) && (!Game)) || ((deltaA < 0) && (Game)))//Positive rotation based on x axis (CCW)
    {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, 0, RotateAccelL, RotateSpeedL, RotateDecelL, 0, 0);
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, turnDistR, RotateAccelL, RotateSpeedL, RotateDecelL, -turnDistL, 0);
    }
    else //Negative rotation based on x axis (CW)
    {
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, 0, RotateAccelL, RotateSpeedL, RotateDecelL, 0, 0);
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, RotateAccelR, RotateSpeedR, RotateDecelR, -turnDistR, RotateAccelL, RotateSpeedL, RotateDecelL, turnDistL, 0);
    }

    UpdateExtPos();
}

void UpdateExtPos()
{
    //Initiate and reset encoder variables
    double leftDist = 0; //Distance traveled by left motor since last updated
    double rightDist = 0; //Distance traveled by right motor since last updated
    double centerDist = 0; //Distance traveled by the center of the robot since last updated
    double arcA = 0; //Arc accomplished by the robot since last updated where the angle is based on the axle of the robot at starting angle
    long encoderValueLRead = 0; //Current reading variable of the left external encoder
    long encoderValueRRead = 0; //Current reading variable of the right external encoder
    long LastEncoderValueLRead = 0; 
    long LastEncoderValueRRead = 0;
    EncoderValueL = 0; //Reset global EncoderValueL attached to interrupt
    EncoderValueR = 0; //Reset global EncoderValueR attached to interrupt

    //Distance running average variables
    double arcTotal = 0;
    double arcs[DistanceArrayLength] = {0};
    int index = 0;
    double arcAverage = 0;

    //Rotate running average variables
    double xDisp = 0;
    double yDisp = 0;
    double xDispTotal = 0;
    double yDispTotal = 0;
    double xDispArr[RotateArrayLength] = {0};
    double yDispArr[RotateArrayLength] = {0};
    double xDispAverage = 0;
    double yDispAverage = 0;

    //Acceleration variables
    int accel = 0;
    int lastAccel = 336;
    int jerk = 0;

    uint8_t depth1 = 0, depth2 = 0;
    //Depth 1 is motor 1 buffer value, depth 2 is motor 2 buffer value
    //Buffer value is the number of commands waiting to be executed by RoboClaw
    //If buffer is 0, last command is currently being executed, and 0x80 means buffer is empty
    while (depth1 != 0x80 && depth2 != 0x80) //Loop until position command is completed
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);

        //accel = analogRead(A0);
        //jerk = abs(accel-lastAccel);
        if (jerk > MaxJerk)
        {
            roboclaw.DutyM1M2(ADDRESS, 0, 0);
            Collision = true;
            XBee.print("Collision detected, jerk measured ");
            XBee.print(jerk);
            XBee.print('!');
            delay(5000);
            break;
        }
        lastAccel = accel;

        uint32_t start = millis();
        uint32_t end = millis();
        while ((end - start) < ReadBufferDelay)
        {
            if (((UpdateCnt >= MaxPulseDist) && DistanceBool) || ((UpdateCnt >= MaxPulseRotate) && !DistanceBool))
            {
                encoderValueLRead = EncoderValueL; //Store left encoder reading as a variable
                encoderValueRRead = EncoderValueR; //Store right encoder reading as a variable
                leftDist = (encoderValueLRead-LastEncoderValueLRead)*MM_PER_CNT_EXT;
                rightDist = (encoderValueRRead-LastEncoderValueRRead)*MM_PER_CNT_EXT;
                centerDist = ((leftDist+rightDist)/2);
                if (Game)
                {
                    arcA = (leftDist-rightDist)/WHEEL_SEP_EXT;
                }
                else
                {
                    arcA = (rightDist-leftDist)/WHEEL_SEP_EXT;
                }

                if (DistanceBool)
                {
                    arcTotal -= arcs[index];
                    arcs[index] = arcA;
                    arcTotal += arcs[index];
                    index++;
                    if (index >= DistanceArrayLength)
                    {
                        index = 0;
                    }
                    arcAverage = arcTotal/DistanceArrayLength;
                    if (fabs(arcA) > 0.8*fabs(arcAverage))
                    {
                        arcA = 0;
                    }
                }

                if (arcA != 0)
                {
                    xDisp = (centerDist/arcA)*(sin(APosExt + arcA)-sin(APosExt));
                    yDisp = (centerDist/arcA)*(cos(APosExt)-cos(APosExt+arcA));
                }
                else
                {
                    xDisp = centerDist*cos(APosExt); //Approximation valid  for a small arcA
                    yDisp = centerDist*sin(APosExt); //Approximation valid  for a small arcA
                }

                if (!DistanceBool)
                {
                    xDispTotal -= xDispArr[index];
                    yDispTotal -= yDispArr[index];
                    xDispArr[index] = xDisp;
                    yDispArr[index] = yDisp;
                    xDispTotal += xDispArr[index];
                    yDispTotal += yDispArr[index];
                    index++;
                    if (index >= RotateArrayLength)
                    {
                        index = 0;
                    }
                    xDispAverage = xDispTotal/RotateArrayLength;
                    yDispAverage = yDispTotal/RotateArrayLength;
                    if (fabs(xDisp) > fabs(xDispAverage))
                    {
                        xDisp = 0;
                    }
                    if (fabs(yDisp) > fabs(yDispAverage))
                    {
                        yDisp = 0;
                    }
                }

                XPosExt += xDisp;
                YPosExt += yDisp;

                APosExt += arcA;

                //Conditions to determine smallest angle
                if (APosExt > PI)
                {
                    APosExt = -2*PI+APosExt;
                }
                else if (APosExt < -PI)
                {
                    APosExt = 2*PI+APosExt;
                }

                //Convert angle relative to axle to the global coordinates
                if (APosExt < 0)
                {
                    APosExtRel = 2*PI+APosExt;
                }
                else
                {
                    APosExtRel = APosExt;
                }

                //Save encoder values for next update
                LastEncoderValueLRead = encoderValueLRead;
                LastEncoderValueRRead = encoderValueRRead;

                UpdateCnt = 0;
            }

            end = millis();
        }
    }
}

void UpdateExtEncoderL()
{
    if (digitalRead(EncoderPinL2) == LOW) 
    {
        EncoderValueL++;
    }
    else
    {
        EncoderValueL--;
    }
    UpdateCnt++;
}

void UpdateExtEncoderR()
{
    if (digitalRead(EncoderPinR2) == LOW) 
    {
        EncoderValueR++;
    }
    else
    {
        EncoderValueR--;
    }
    UpdateCnt++;
}