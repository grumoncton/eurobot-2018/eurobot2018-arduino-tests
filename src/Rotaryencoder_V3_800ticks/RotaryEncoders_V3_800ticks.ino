#include <SimpleTimer.h>
#include <Arduino.h>
SimpleTimer timer;

//Robot Dimensions
#define cprExt 1600 //External encoder ticks per rotation
#define extWheelDia 63.4 //External encoder wheel diameter (mm)
#define extWheelDist 202.8 //Distance between external encoder centers (mm)
float extDistPerCount = (extWheelDia*PI) / cprExt;
float extRadPerCount = extDistPerCount / (extWheelDist);

//External Encoders Variables
int encoderPinL1 = 3;
int encoderPinL2 = 2;
int encoderPinR1 = 19;
int encoderPinR2 = 18;
volatile long encoderL = 0;
volatile long encoderR = 0;
long lastEncoderL = 0;
long lastEncoderR = 0;


volatile int moveL = 0;
volatile int moveR = 0;

void setup() {
  Serial.begin(115200);

  //External encoders Setup
  pinMode(encoderPinL1, INPUT);
  pinMode(encoderPinL2, INPUT);
  pinMode(encoderPinR1, INPUT);
  pinMode(encoderPinR2, INPUT);
  digitalWrite(encoderPinL1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPinL2, HIGH); //turn pullup resistor on
  digitalWrite(encoderPinR1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPinR2, HIGH); //turn pullup resistor on
  attachInterrupt(0, interrupt0, RISING);
  attachInterrupt(1, interrupt1, RISING);
  attachInterrupt(4, interrupt4, RISING);
  attachInterrupt(5, interrupt5, RISING);
  timer.setInterval(0.25,updatePosition);  //Update position every 1ms

}

void loop() {
  // put your main code here, to run repeatedly:
  timer.run();
}

void updatePosition() {
  //Will execute at set interval. Smaller the interval, more precise the approximation
  moveL = encoderL - lastEncoderL;  lastEncoderL = encoderL;
  moveR = encoderR - lastEncoderR;  lastEncoderR = encoderR;



  Serial.print(" Encoder value:");Serial.print(encoderL);Serial.print(", ");Serial.print(encoderR);Serial.println(",");


}

//External Encoders Interrupts
//===================================================
//Left External Encoder Count
//---------------------------------------------------
void interrupt0() {      //LSB goes from LOW to HIGH
  if(digitalRead(3)==LOW) {encoderL++;}
  else                    {encoderL--;}
}
void interrupt1() {      //MSB goes from LOW to HIGH
  if(digitalRead(2)==LOW) {encoderL--;}
  else                    {encoderL++;}
}
//---------------------------------------------------
//Right External Encoder Count
//---------------------------------------------------
void interrupt4() {      //LSB goes from LOW to HIGH
  if(digitalRead(18)==LOW){encoderR++;}
  else                    {encoderR--;}
}
void interrupt5() {      //MSB goes from LOW to HIGH
  if(digitalRead(19)==LOW){encoderR--;}
  else                    {encoderR++;}
}
