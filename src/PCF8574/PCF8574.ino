#include <Wire.h>

int LED0 = 0; int LED7 = 0;
int LED1 = 0; int LED6 = 0;
int LED2 = 0; int LED5 = 0;
int LED3 = 0; int LED4 = 0;

int del = 50;

void setup() {
  // put your setup code here, to run once:
  Wire.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  uint8_t val = LED0 + 2*LED1 + 4*LED2 + 8*LED3 + 16*LED4 + 32*LED5 + 64*LED6 + 128*LED7;

    Wire.beginTransmission(0x20);
    Wire.write(val);
    Wire.endTransmission();
    delay(del);
}
