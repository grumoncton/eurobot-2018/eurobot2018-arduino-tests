//-------------------------------------------------------------------------------
//  Code pour les ultrasons des robots de GRUM
//  Si le master request du data, le nano read la distance des 8 capteurs pi
//  les envoient au RPi o� la value de la byte est la distance en mm (255 = 255mm)
//-------------------------------------------------------------------------------
#include <Arduino.h>
#include <Wire.h>                       //Include la I2C library
int trigPin[] = {2, 4, 6, 8, 10, 12, A0, A2};             //Define les digital pins du Arduino pour les trigger des capteurs
int echoPin[] = {3, 5, 7, 9, 11, 13, A1, A3};             //Define les digital pins du Arduino pour les echo des capteurs
int distINT[] = {0, 0, 0, 0, 0, 0, 0, 0};                //Distance en mm sous forme de INT
uint8_t distBYTE[] = {0, 0, 0, 0, 0, 0, 0, 0};              //Distance en mm sous forme de byte
long duration[] = {0, 0, 0, 0, 0, 0, 0, 0};              //Duration en microseconds
int val;

void light();
void dataRequest();
void setup() {
  for (int i = 0; i <= 7; i++) {                    //Define les digital outputs. Triggers = OUTPUT. Echos = INPUT
    pinMode(trigPin[i], OUTPUT); pinMode(echoPin[i], INPUT);
  }
  Wire.begin(0x20);
  Serial.begin(9600);
  Wire.onRequest(dataRequest);                      //Quand le Master request du data, �a execute la fonction dataRequest
}

void loop() {
  for (int i = 0; i <= 7; i++) {                    //For loop pour calculer la distance de tous les capteurs, envoi la distance de chacun en mm sous forme de BYTE vers le master (RPi)
    digitalWrite(trigPin[i], LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin[i], HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin[i], LOW);
    duration[i] = pulseIn(echoPin[i], HIGH, 10000);  //Temps en microsecondes que le son prend pour frapper l'objet et revenir au capteur
    pinMode(echoPin[i], OUTPUT);
    digitalWrite(echoPin[i], LOW);
    pinMode(echoPin[i], INPUT);
    distINT[i] = (duration[i] / 2) / 2.91;          //Distance (sous forme de INT) en mm entre capteur et objet
    if (distINT[i] > 255) {                         //Limit la distance a 255 pour pouvoir la convertir en BYTE
      distINT[i] = 0;
    }
    distBYTE[i] = (byte)distINT[i];                 //Converti la distance de INT � BYTE
  }
 light();
}

void light() {
  val = 255;
  for (int i = 0; i <= 7; i++) {
    if (distBYTE[i] != 0) {
      val = val - pow(2, i);
    }
  }
  Wire.beginTransmission(0x27);
  Wire.write(val);
  Wire.endTransmission();
}
void dataRequest() {
  for (int i = 0; i <= 7; i++) {
    Wire.write(distBYTE[i]);
  }
}
