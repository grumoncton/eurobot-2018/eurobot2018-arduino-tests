/*    Code pour la nano sur le spinner arm
 *    La seule tâche du code est de recevoir une byte par I2C
 *    puis de executer une tâche
 *    Tâches (numéro est la byte reçu) :
 *      0 : Place le spinner vertical
 *      1 : Place le spinner horizontal
 *      2 : Tourne le spinner 90 degree CW
 *      3 : Tourne le spinner 90 degree CCW
 *      4 : Tourne le spinner 180 degree CW
 *    Il a aussi une state value qui est 0 si il execute une tâche
 *    et 1 si la tâche est fini. Le master peut lire la byte avec
 *    une read function.
 */
#include <DRV8825.h>
#include <Servo.h>
#include <Wire.h>

#define servoUp 0 //Defining names
#define servoDown 1
#define stepperCW90 2
#define stepperCCW90 3
#define stepperCW180 4
#define inProgress 0
#define finished 1

#define spr 200   //Steps Per Revolution
#define rpm 600   //stepper RPM
#define enaPin 10 //Enable Pin
#define m0 9      //Mode 0
#define m1 8      //Mode 1
#define m2 7      //Mode 2
#define stepPin 6 //Step Pin
#define dirPin 5  //Direction Pin
#define mSteps 8  //microsteps

DRV8825 stepper(spr, dirPin, stepPin, enaPin, m0, m1, m2);  //Object pour le stepper
Servo servo1; //Object pour le servo1
Servo servo2; //Object pour le servo2

uint8_t receivedByte = 0;               //Command received from RPi
bool newByte = false;                   //True if new command received
uint8_t state = 0;                      //Data to send to RPi
uint8_t ver1 = 135; uint8_t ver2 = 45;  //Vertical servo position
uint8_t hor1 = 45; uint8_t hor2 = 135;  //Horizontal servo position

void setup() {
  Wire.begin(0x20);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);
  
  stepper.begin(rpm,mSteps);
  stepper.disable();
  
  servo1.attach(11);  //Servo 1 on pin 11
  servo2.attach(12);  //Servo 2 on pin 12
  servo1.write(ver1); //Arm vertical to start match
  servo2.write(ver2); //^
}

void loop() { 
  if (newByte) {
    newByte = false;
    switch (receivedByte) {
      case servoUp:
        servo1.write(135);
        servo2.write(45);                  
        state = finished;
        break;
        
      case servoDown:        
        for (int pos1 = 135; pos1 >= 45; pos1--) {
          servo1.write(pos1);              
          servo2.write(180-pos1);
          delay(8);                       
        }
        state = finished;
        break;
        
      case stepperCW90:
        stepper.enable();
        stepper.rotate(1270);
        //stepper.disable();
        state = finished;
        break;
        
      case stepperCCW90:
        stepper.enable();
        stepper.rotate(-1270);
        //stepper.disable();
        state = finished;
        break;
        
      case stepperCW180:
        stepper.enable();
        stepper.rotate(2540);
        //stepper.disable();
        state = finished;
        break;      
    }
  }
}

void dataReceive() {
  receivedByte = Wire.read();
  newByte = true;
  state = inProgress;
  /*
     receivedByte = 0, servos -> vertical
     receivedByte = 1, servos -> horizontal
     receivedByte = 2, turn 90 CW
     receivedByte = 3, turn 90 CCW
     receivedByte = 4, turn 180 CW
  */
}

void dataRequest() {
  Wire.write(state);
}

