#include <Arduino.h>
#include <Wire.h>
#include "address-map.h"

double xPos = 0;
double yPos = 420.69;
double aPos = PI;

struct Coord {
  uint16_t x;
  uint16_t y;
};

Coord path[8];
uint8_t pathIndex = 0;
uint8_t pathLength = 0;

void recievePath(uint8_t bytes) {
  pathIndex = 0;
  pathLength = bytes / 4;
  Serial.println(pathLength);

  for (uint8_t i = 0; i < pathLength; i++) {
    Coord coord;

    coord.x = (Wire.read() << 8) | Wire.read();
    coord.y = (Wire.read() << 8) | Wire.read();

    path[i] = coord;
  }
  for (uint8_t i = 0; i < 8; i++) {
    Coord coord = path[i];

    Serial.print(coord.x);
    Serial.print(" ");
    Serial.print(coord.y);
    Serial.println();
  }
}
volatile uint8_t _instruction;
void recieveHandler(int bytes) {
  uint8_t instruction = Wire.read();
  if (instruction == 0x00) return;

  _instruction = instruction;
}

void requestHandler() {

  switch(_instruction) {

    // Send position
    case 0x11: {
      // Serial.println("Sending position");
      byte result[5] = {0};
      uint16_t xPosInt = (uint16_t) (xPos * 20);
      uint16_t yPosInt = (uint16_t) (yPos * 20);
      result[0] = xPosInt >> 8;
      result[1] = xPosInt % (1 << 8);
      result[2] = yPosInt >> 8;
      result[3] = yPosInt % (1 << 8);
      result[4] = aPos * (1 << 8) / (2 * PI);
      for (uint8_t i = 0; i < 5; i++) {
        Wire.write(result[i]);
      }
      break;
    }

    // Recieve path
    case 0x12: {
      // recievePath(bytes);
      break;
    }
  }
}

void setup() {
  Wire.begin(ADDRESS_WHEELS);
  Wire.onReceive(recieveHandler);
  Wire.onRequest(requestHandler);
  Serial.begin(9600);
}

void loop() {
}

