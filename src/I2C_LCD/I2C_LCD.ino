#include <Wire.h>                 // Pour la communication I2C
#include <LiquidCrystal_I2C.h>    // Pour la LCD

#define I2C_ADDR    0x27        // Défini l'adresse I2C de l'écran
#define BACKLIGHT_PIN  3     // Défini la pin qui contrôle le retroéclairage
#define En_pin  2               // Définition des pins du LCD
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7
LiquidCrystal_I2C lcd(I2C_ADDR, En_pin, Rw_pin, Rs_pin, D4_pin, D5_pin, D6_pin, D7_pin);

void setup() {
  lcd.begin (16, 2);
  lcd.setBacklightPin(BACKLIGHT_PIN, POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.home (); // go home
}
void loop() {
  lcd.setCursor(0,0);
  lcd.print("Score du robot :");
  lcd.setCursor(0,1);
  lcd.print("500pts");
}

