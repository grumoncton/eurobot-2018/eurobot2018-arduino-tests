#include <Encoder_Buffer.h>
#include <SPI.h>

//#define EncoderCSL 8

#define EncoderCSR 7

//int count =0;
//long encoderLReading = 0;
long encoderRReading = 0;

// Encoder_Buffer Encoder1(EncoderCSL);
Encoder_Buffer EncoderR(EncoderCSR);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  SPI.begin();
//  Encoder1.initEncoder();
  EncoderR.initEncoder();
}

void loop() {
//  encoderLReading = Encoder1.readEncoder();//Read Encoder
  encoderRReading = EncoderR.readEncoder();//Read Encoder
//  Serial.print(encoderLReading);Serial.print(" ");
  Serial.println(encoderRReading);
 // delay(500);
}
