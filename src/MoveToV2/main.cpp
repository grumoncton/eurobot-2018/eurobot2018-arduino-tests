#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void MoveTo(double x, double y, double a, int start, int end);
void AdjustmentTurn(double deltaA);

RoboClaw roboclaw(&Serial3, 10000);

//Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
#define CPR_EXT 1600 //External encoder ticks per rotation
#define WHEEL_DIA_EXT 63.4 //External encoder wheel diameter (mm)
#define WHEEL_DIA_INT 64.53 //Internal encoder wheel diameter (mm)
#define WHEEL_SEP_EXT 202.8 //Distance between external encoder centers (mm)
#define WHEEL_SEP_INT 286 //Distance between internal encoder centers (mm)
#define ADDRESS 0x80
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT;
double MM_PER_CNT_INT = (WHEEL_DIA_INT*PI)/CPR_INT;

//Movement quantities mm/s mm/s^2
#define SPEED 200
#define ACCEL 200
#define DECEL 200

//Movement quantities calculated counts/s counts/s^2
double Speed = SPEED/MM_PER_CNT_INT;
double Accel = ACCEL/MM_PER_CNT_INT;
double Decel = DECEL/MM_PER_CNT_INT;

//Starting positions in mmm and degrees
#define X_START 0
#define Y_START 0
#define A_START 0

//Actual position variables in mm and degrees
double XPos = X_START;
double YPos = Y_START;
double APos = A_START;
double APosRad = APos*(PI/180);

//Movement variables
double XDist;
double XDistCountInt;
double XDistCountExt;
double YDist;
double YDistCountInt;
double YDistCountExt;
double TDistCountExt;
double AdjustedA;
double DeltaA;

int mode;
bool state = true;

void setup()
{
    Serial.begin(38400);
    Serial3.begin(38400);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS);
    delay(100);
    Serial.println();
    Serial.println("Current Position: 0, 0, 0");
    Serial.println("================================================================================================");
    Serial.println("Initialized");
    Serial.println("================================================================================================");
}

void loop()
{
    if (state)
    {
        MoveTo(0, 2650, 0, 3, 3);
        MoveTo(0, 0, 0, 3, 3);
        //MoveTo(0, 2615, 0, 3, 3);
        //MoveTo(0, 0, 0, 3, 3);
        state = false;
    }


    //Encoder Readings
    // uint8_t status1,status2;
    // bool valid1,valid2;
    // int32_t enc1= roboclaw.ReadEncM1(ADDRESS, &status1, &valid1);
    // int32_t enc2 = roboclaw.ReadEncM2(ADDRESS, &status2, &valid2);
    // Serial.print("Encoder 1:");
    // if (valid1)
    // {
    //     Serial.print(enc1);
    //     Serial.print(" ");
    //     Serial.print(status1);
    //     Serial.print(" ");
    // }
    // else
    // {
    //     Serial.print("invalid ");
    // }
    // Serial.print("Encoder2:");
    // if (valid2)
    // {
    //     Serial.print(enc2);
    //     Serial.print(" ");
    //     Serial.println(status2);
    // }
    // else
    // {
    //     Serial.println("invalid ");
    // }
}

//Motor 1 = 1, Motor 2 = 2, Simultaneous = 3
void MoveTo(double x, double y, double a, int start, int end)
{
    double aRad = PI*(a/180);
    mode = 10*start+end;

    XDist = x-XPos;
    XDistCountInt = XDist/MM_PER_CNT_INT;
    XDistCountExt = XDist/MM_PER_CNT_EXT;
    YDist = y-YPos;
    YDistCountInt = YDist/MM_PER_CNT_INT;
    YDistCountExt = YDist/MM_PER_CNT_EXT;
    Serial.print("XDist ");
    Serial.print(XDist);
    Serial.print(" YDist ");
    Serial.println(YDist);

    switch (mode)
    {
        case 11:
            break;

        case 12:
            break;

        case 13:
            break;

        case 21:
            break;

        case 22:
            break;

        case 23:
            break;

        case 31:
            break;

        case 32:
            break;

        case 33:
        double travelDist;
            if (XDist >= 0 && YDist > 0)
            {
                if (x == XPos)
                {
                    AdjustedA = 0;
                }
                else
                {
                    AdjustedA = atan2(fabs(XDistCountInt), fabs(YDistCountInt));
                }
            }
            else if (XDist > 0 && YDist <= 0)
            {
                if (x == XPos)
                {
                    AdjustedA = PI;
                }
                else
                {
                    AdjustedA = atan2(fabs(YDistCountInt), fabs(XDistCountInt))+PI/2;
                }
            }
            else if (XDist <= 0 && YDist < 0)
            {
                AdjustedA = atan2(fabs(XDistCountInt), fabs(YDistCountInt))+PI;
            }
            else if (XDist < 0 && YDist >= 0)
            {
                AdjustedA = atan2(fabs(YDistCountInt), fabs(XDistCountInt))+(3*PI)/2;
            }
            DeltaA = AdjustedA-APosRad;
            Serial.print("First adjustment turn: AdjustedA ");
            Serial.print(AdjustedA);
            Serial.print("rad, ");
            Serial.print("DeltaA ");
            Serial.print(DeltaA);
            Serial.print("rad, ");

            if (DeltaA != 0)
            {
                AdjustmentTurn(DeltaA);
                APosRad = AdjustedA;
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
            }
            else
            {
                Serial.println("deltaA NONE, turnDist NONE");
            }

            travelDist = hypot(XDistCountInt, YDistCountInt);
            if (travelDist != 0)
            {
                roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, travelDist, Accel, Speed, Decel, travelDist, 0);
                uint8_t depth1 = 0, depth2 = 0;
                while (depth1 != 0x80 && depth2 != 0x80)
                {
                    roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
                    delay(100);
                }
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
                Serial.print("Travel: travelDist: ");
                Serial.println(travelDist);
            }
            else
            {
                Serial.println("Travel: travelDist NONE");
            }
            XPos=x;
            YPos=y;

            DeltaA = aRad-APosRad;
            Serial.print("Second adjustment turn: DeltaA ");
            Serial.print(DeltaA);
            Serial.print("rad, ");

            if (DeltaA != 0)
            {
                AdjustmentTurn(DeltaA);
                APosRad = aRad;
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
            }
            else
            {
                Serial.println("deltaA NONE, turnDist NONE");
            }

            Serial.print("Current Position: ");
            Serial.print(XPos);
            Serial.print(", ");
            Serial.print(YPos);
            Serial.print(", ");
            Serial.println((APosRad/PI)*180);
            Serial.println("------------------------------------------------------------------------------------------------");
            Serial.println("Position reached");
            Serial.println("------------------------------------------------------------------------------------------------");
            break;
    }
}

void AdjustmentTurn(double deltaA)
{
    double turnDist;
    if (deltaA >= 0 && deltaA <= PI) //CW
    {
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CW");
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, -turnDist, Accel, Speed, Decel, turnDist, 0);
    }
    else if (deltaA > 0 && deltaA > PI) //CCW
    {
        deltaA = 2*PI-deltaA;
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CCW");
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, turnDist, Accel, Speed, Decel, -turnDist, 0);
    }
    else if (deltaA < 0 && deltaA >= -PI) //CCW
    {
        deltaA = fabs(deltaA);
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CCW");
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, turnDist, Accel, Speed, Decel, -turnDist, 0);
    }
    else if (deltaA < 0 && deltaA < -PI) //CW
    {
        deltaA += 2*PI;
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CW");
        roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, -turnDist, Accel, Speed, Decel, turnDist, 0);
    }
    uint8_t depth1 = 0, depth2 = 0;
    while (depth1 != 0x80 && depth2 != 0x80)
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(100);
    }
}
