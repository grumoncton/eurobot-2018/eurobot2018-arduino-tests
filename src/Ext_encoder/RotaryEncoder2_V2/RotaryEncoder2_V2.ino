int encoderPinL1 = 2;
int encoderPinL2 = 3;
int encoderPinD1 = 19;
int encoderPinD2 = 18; // **IMPORTANT** pour la commande attachInterrupt seulement les pin 2,3,21,20,19,18 sur le mega peuvent fonctionner voir arduino.cc**
volatile int lastEncodedL = 0;
volatile int lastEncodedD = 0;
volatile long encoderValueL = 0;
volatile long encoderValueD = 0;

long lastencoderValueL = 0;
long lastencoderValueD = 0;

int lastMSB_L = 0;
int lastLSB_D = 0;

void updateEncoder(){
  int MSB_L = digitalRead(encoderPinL1); //MSB_L = most significant bit left motor
  int LSB_L = digitalRead(encoderPinL2); //LSB_L = least significant bit left motor
  int MSB_D = digitalRead(encoderPinD1); //MSB_D = most significant bit right motor
  int LSB_D = digitalRead(encoderPinD2); //LSB_D = least significant bit right motor
  
  int encodedL = (MSB_L << 1) |LSB_L; //converting the 2 pin of the left motor value to single number
  int encodedD = (MSB_D << 1) |LSB_D; //converting the 2 pin of the right motor value to single number
  int sumML  = (lastEncodedL << 2) | encodedL; //adding it to the previous encoded value for left motor
  int sumMD  = (lastEncodedD << 2) | encodedD; //adding it to the previous encoded value for right motor
 
  if(sumML == 0b1101 || sumML == 0b0100 || sumML == 0b0010 || sumML == 0b1011) encoderValueL ++;
  if(sumML == 0b1110 || sumML == 0b0111 || sumML == 0b0001 || sumML == 0b1000) encoderValueL --;
  if(sumMD == 0b1101 || sumMD == 0b0100 || sumMD == 0b0010 || sumMD == 0b1011) encoderValueD ++;
  if(sumMD == 0b1110 || sumMD == 0b0111 || sumMD == 0b0001 || sumMD == 0b1000) encoderValueD --;
 
  lastEncodedL = encodedL; //store this value for next time
  lastEncodedD = encodedD; 
  
}

void setup() {
  Serial.begin (9600);
 
  pinMode(encoderPinL1, INPUT); 
  pinMode(encoderPinL2, INPUT);
  pinMode(encoderPinD1, INPUT); 
  pinMode(encoderPinD2, INPUT);
 
  
digitalWrite(encoderPinL1, HIGH); //turn pullup resistor on
digitalWrite(encoderPinL2, HIGH); //turn pullup resistor on
digitalWrite(encoderPinD1, HIGH); //turn pullup resistor on
digitalWrite(encoderPinD2, HIGH); //turn pullup resistor on
 
  //call updateEncoder() when any high/low changed seen
  //on interrupt 0 (pin 2), interrupt 1 (pin 3),interrupt 2 (pin 21), interrupt 3 (pin 20), interrupt 4 (pin 19) and interrupt 5 (pin 18)
  attachInterrupt(0, updateEncoder, CHANGE); 
  attachInterrupt(1, updateEncoder, CHANGE);
  attachInterrupt(4, updateEncoder, CHANGE); 
  attachInterrupt(5, updateEncoder, CHANGE);

 Serial.println("left value----------------------------right value");
}
 
void loop(){ 
  //Do stuff here
 
  Serial.print(encoderValueL);
  Serial.print("----------------------------");
  Serial.println(encoderValueD);
}
 
 
