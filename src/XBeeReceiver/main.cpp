#include <Arduino.h>

#define XBeeRec Serial2

String valRead;

void setup()
{
    XBeeRec.begin(9600);
    Serial.begin(9600);
}

void loop()
{
    if (XBeeRec.available())
    {
        valRead = XBeeRec.readStringUntil('!');
        Serial.println(valRead);
    }
}
