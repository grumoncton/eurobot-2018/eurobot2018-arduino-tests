enum Direction {
  FORWARDS = 1,
  BACKWARDS = -1,
};

enum TableSide {
  ORANGE = 0x4F,
  GREEN = 0x47,
};

enum Instruction {
  NONE = 0x00,
  GET_STATE = 0x01,
  SET_SIDE = 0x02,
  STOP = 0x10,
  GET_POS = 0x11,
  MOVE_TO = 0x12,
  ROTATE_TO = 0x13,
  MOVE_TO_PATH = 0x14,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  STOPPED = 2,
  DONE = 3,
};

