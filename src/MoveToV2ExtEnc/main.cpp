#include <Arduino.h>
#include <RoboClaw.h>
#include <math.h>

//Function decleration
void MoveTo(double x, double y, double a, int start, int end);
void AdjustmentTurn(double deltaA);
void UpdateExtPos();
void UpdateExtEncoder();

RoboClaw roboclaw(&Serial3, 10000);

//Robot Dimensions
#define CPR_INT 979.2 //Internal encoder counts per rotation (20.4*48)
#define CPR_EXT 1600 //External encoder ticks per rotation
#define WHEEL_DIA_EXT 63.4 //External encoder wheel diameter (mm)
#define WHEEL_DIA_INT 74 //Internal encoder wheel diameter (mm)
#define WHEEL_SEP_EXT 202.8 //Distance between external encoder centers (mm)
#define WHEEL_SEP_INT 287.4 //Distance between internal encoder centers (mm)
#define ADDRESS 0x80
double MM_PER_CNT_EXT = (WHEEL_DIA_EXT*PI)/CPR_EXT;
double MM_PER_CNT_INT = (WHEEL_DIA_INT*PI)/CPR_INT;

//Movement quantities mm/s mm/s^2
#define SPEED 200
#define ACCEL 200
#define DECEL 200

//Movement quantities calculated counts/s counts/s^2
double Speed = SPEED/MM_PER_CNT_INT;
double Accel = ACCEL/MM_PER_CNT_INT;
double Decel = DECEL/MM_PER_CNT_INT;
double AccelDist = (Speed*Speed)/(2*Accel);
double DecelDist = (Speed*Speed)/(2*Decel);
double TotalDist = AccelDist+DecelDist;

//Starting positions in mmm and degrees
#define X_START 0
#define Y_START 0
#define A_START 0

//Actual position variables in mm and degrees
double XPos = X_START;
double YPos = Y_START;
double APos = A_START;
double APosRad = APos*(PI/180);

//External Encoder Position Variables
double XPosExt;
double YPosExt;
double APosExt;
double LeftDist;
double RightDist;
double CenterDist;
double ArcA;

//Movement variables
double XDist;
double XDistCountInt;
double XDistCountExt;
double YDist;
double YDistCountInt;
double YDistCountExt;
double TDistCountExt;
double AdjustedA;
double DeltaA;

//External Encoder Parameters
int encoderPinL1 = 2;
int encoderPinL2 = 3;
int encoderPinR1 = 18;
int encoderPinR2 = 19; // **IMPORTANT** pour la commande attachInterrupt seulement les pin 2,3,18,19,20,21 sur le mega peuvent fonctionner voir arduino.cc**
volatile int lastEncodedL = 0;
volatile int lastEncodedR = 0;
volatile long encoderValueL = 0;
volatile long encoderValueR = 0;
long lastencoderValueL = 0;
long lastencoderValueR = 0;

int mode;
bool state = true;

void setup()
{
    Serial.begin(38400);
    Serial3.begin(38400);
    roboclaw.begin(38400);
    roboclaw.ResetEncoders(ADDRESS);
    delay(100);
    Serial.println();
    Serial.println("Current Position: 0, 0, 0");
    Serial.println("================================================================================================");
    Serial.println("Initialized");
    Serial.println("================================================================================================");

    pinMode(encoderPinL1, INPUT);
    pinMode(encoderPinL2, INPUT);
    pinMode(encoderPinR1, INPUT);
    pinMode(encoderPinR2, INPUT);
    digitalWrite(encoderPinL1, HIGH); //turn pullup resistor on
    digitalWrite(encoderPinL2, HIGH); //turn pullup resistor on
    digitalWrite(encoderPinR1, HIGH); //turn pullup resistor on
    digitalWrite(encoderPinR2, HIGH); //turn pullup resistor on
    //call UpdateExtEncoder() when any high/low changed seen
    //on interrupt 0 (pin 2), interrupt 1 (pin 3),interrupt 2 (pin 21), interrupt 3 (pin 20), interrupt 4 (pin 19) and interrupt 5 (pin 18)
    attachInterrupt(digitalPinToInterrupt(2), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(3), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(18), UpdateExtEncoder, CHANGE);
    attachInterrupt(digitalPinToInterrupt(19), UpdateExtEncoder, CHANGE);
}

void loop()
{
    if (state)
    {
        MoveTo(0, 2650, 0, 3, 3);
        state = false;
    }
    //External Encoder Readings
    // Serial.print(encoderValueL);
    // Serial.print("|----------------------------|");
    // Serial.println(encoderValueR);

    //Internal Encoder Readings
    // uint8_t status1,status2;
    // bool valid1,valid2;
    // int32_t enc1= roboclaw.ReadEncM1(ADDRESS, &status1, &valid1);
    // int32_t enc2 = roboclaw.ReadEncM2(ADDRESS, &status2, &valid2);
    // Serial.print("Encoder 1:");
    // if (valid1)
    // {
    //     Serial.print(enc1);
    //     Serial.print(" ");
    //     Serial.print(status1);
    //     Serial.print(" ");
    // }
    // else
    // {
    //     Serial.print("invalid ");
    // }
    // Serial.print("Encoder2:");
    // if (valid2)
    // {
    //     Serial.print(enc2);
    //     Serial.print(" ");
    //     Serial.println(status2);
    // }
    // else
    // {
    //     Serial.println("invalid ");
    // }
}

//Motor 1 = 1, Motor 2 = 2, Simultaneous = 3
//dir 0 = forward, 1 = backwards
void MoveTo(double x, double y, double a, int start, int end, bool dir)
{
    double aRad = PI*(a/180);
    mode = 10*start+end;

    XDist = x-XPos;
    XDistCountInt = XDist/MM_PER_CNT_INT;
    XDistCountExt = XDist/MM_PER_CNT_EXT;
    YDist = y-YPos;
    YDistCountInt = YDist/MM_PER_CNT_INT;
    YDistCountExt = YDist/MM_PER_CNT_EXT;
    Serial.print("XDist ");
    Serial.print(XDist);
    Serial.print(" YDist ");
    Serial.println(YDist);

    switch (mode)
    {
        case 11:
            break;

        case 12:
            break;

        case 13:
            break;

        case 21:
            break;

        case 22:
            break;

        case 23:
            break;

        case 31:
            break;

        case 32:
            break;

        case 33:
        double travelDist;
            if (XDist >= 0 && YDist > 0)
            {
                if (x == XPos)
                {
                    AdjustedA = 0;
                }
                else
                {
                    AdjustedA = atan2(fabs(XDistCountInt), fabs(YDistCountInt));
                }
            }
            else if (XDist > 0 && YDist <= 0)
            {
                if (x == XPos)
                {
                    AdjustedA = PI;
                }
                else
                {
                    AdjustedA = atan2(fabs(YDistCountInt), fabs(XDistCountInt))+PI/2;
                }
            }
            else if (XDist <= 0 && YDist < 0)
            {
                AdjustedA = atan2(fabs(XDistCountInt), fabs(YDistCountInt))+PI;
            }
            else if (XDist < 0 && YDist >= 0)
            {
                AdjustedA = atan2(fabs(YDistCountInt), fabs(XDistCountInt))+(3*PI)/2;
            }
            DeltaA = AdjustedA-APosRad;
            Serial.print("First adjustment turn: AdjustedA ");
            Serial.print(AdjustedA);
            Serial.print("rad, ");
            Serial.print("DeltaA ");
            Serial.print(DeltaA);
            Serial.print("rad, ");

            if (DeltaA != 0)
            {
                AdjustmentTurn(DeltaA);
                APosRad = AdjustedA;
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
            }
            else
            {
                Serial.println("deltaA NONE, turnDist NONE");
            }

            travelDist = hypot(XDistCountInt, YDistCountInt);
            if (travelDist != 0)
            {
                encoderValueL = 0;
                encoderValueR = 0;
                XPosExt = 0;
                YPosExt = 0;
                APosExt = 0;
                //roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, travelDist, Accel, Speed, Decel, travelDist, 0);
                if (TotalDist > travelDist)
                {
                    AccelDist = (AccelDist/TotalDist)*travelDist;
                    DecelDist = (DecelDist/TotalDist)*travelDist;
                }
                else
                {
                    AccelDist = travelDist-DecelDist;
                }
                roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Accel, Speed, AccelDist, Speed, AccelDist, 0);
                roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Decel, Speed, DecelDist, Speed, DecelDist, 0);
                roboclaw.DutyM1M2(ADDRESS, 0, 0);
                uint8_t depth1 = 0, depth2 = 0;
                while (depth1 != 0x80 && depth2 != 0x80)
                {
                    roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
                    //UpdateExtPos();
                    delay(100);
                }
                delay(750);
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
                Serial.print("External Encoder Position: ");
                Serial.print(XPosExt);
                Serial.print(" ");
                Serial.print(YPosExt);
                Serial.print(" ");
                Serial.println((APosExt/PI)*180);
                Serial.print("Travel: travelDist ");
                Serial.println(travelDist);
            }
            else
            {
                Serial.println("Travel: travelDist NONE");
            }
            XPos=x;
            YPos=y;

            DeltaA = aRad-APosRad;
            Serial.print("Second adjustment turn: DeltaA ");
            Serial.print(DeltaA);
            Serial.print("rad, ");

            if (DeltaA != 0)
            {
                AdjustmentTurn(DeltaA);
                APosRad = aRad;
                roboclaw.ResetEncoders(ADDRESS);
                delay(100);
            }
            else
            {
                Serial.println("deltaA NONE, turnDist NONE");
            }

            Serial.print("Current Position: ");
            Serial.print(XPos);
            Serial.print(", ");
            Serial.print(YPos);
            Serial.print(", ");
            Serial.println((APosRad/PI)*180);
            Serial.println("------------------------------------------------------------------------------------------------");
            Serial.println("Position reached");
            Serial.println("------------------------------------------------------------------------------------------------");
            break;
    }
}

void AdjustmentTurn(double deltaA)
{
    double turnDist;
    if (deltaA >= 0 && deltaA <= PI) //CW
    {
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CW");
        //roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, -turnDist, Accel, Speed, Decel, turnDist, 0);
        if (TotalDist > turnDist)
        {
            AccelDist = (AccelDist/TotalDist)*turnDist;
            DecelDist = (DecelDist/TotalDist)*turnDist;
        }
        else
        {
            AccelDist = turnDist-DecelDist;
        }
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Accel, -Speed, AccelDist, Speed, AccelDist, 0);
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Decel, -Speed, DecelDist, Speed, DecelDist, 0);
        roboclaw.DutyM1M2(ADDRESS, 0, 0);
    }
    else if (deltaA > 0 && deltaA > PI) //CCW
    {
        deltaA = 2*PI-deltaA;
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CCW");
        //roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, turnDist, Accel, Speed, Decel, -turnDist, 0);
        if (TotalDist > turnDist)
        {
            AccelDist = (AccelDist/TotalDist)*turnDist;
            DecelDist = (DecelDist/TotalDist)*turnDist;
        }
        else
        {
            AccelDist = turnDist-DecelDist;
        }
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Accel, Speed, AccelDist, -Speed, AccelDist, 0);
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Decel, Speed, DecelDist, -Speed, DecelDist, 0);
        roboclaw.DutyM1M2(ADDRESS, 0, 0);
    }
    else if (deltaA < 0 && deltaA >= -PI) //CCW
    {
        deltaA = fabs(deltaA);
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CCW");
        //roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, turnDist, Accel, Speed, Decel, -turnDist, 0);
        if (TotalDist > turnDist)
        {
            AccelDist = (AccelDist/TotalDist)*turnDist;
            DecelDist = (DecelDist/TotalDist)*turnDist;
        }
        else
        {
            AccelDist = turnDist-DecelDist;
        }
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Accel, Speed, AccelDist, -Speed, AccelDist, 0);
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Decel, Speed, DecelDist, -Speed, DecelDist, 0);
        roboclaw.DutyM1M2(ADDRESS, 0, 0);
    }
    else if (deltaA < 0 && deltaA < -PI) //CW
    {
        deltaA += 2*PI;
        turnDist = (PI*WHEEL_SEP_INT*(deltaA/(2*PI)))/MM_PER_CNT_INT;
        Serial.print("deltaA ");
        Serial.print(deltaA);
        Serial.print("rad, turnDist ");
        Serial.print(turnDist);
        Serial.println("rad CW");
        //roboclaw.SpeedAccelDeccelPositionM1M2(ADDRESS, Accel, Speed, Decel, -turnDist, Accel, Speed, Decel, turnDist, 0);
        if (TotalDist > turnDist)
        {
            AccelDist = (AccelDist/TotalDist)*turnDist;
            DecelDist = (DecelDist/TotalDist)*turnDist;
        }
        else
        {
            AccelDist = turnDist-DecelDist;
        }
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Accel, -Speed, AccelDist, Speed, AccelDist, 0);
        roboclaw.SpeedAccelDistanceM1M2(ADDRESS, Decel, -Speed, DecelDist, Speed, DecelDist, 0);
        roboclaw.DutyM1M2(ADDRESS, 0, 0);
    }
    uint8_t depth1 = 0, depth2 = 0;
    while (depth1 != 0x80 && depth2 != 0x80)
    {
        roboclaw.ReadBuffers(ADDRESS, depth1, depth2);
        delay(100);
    }
    delay(750);
}

void UpdateExtPos()
{
    // Serial.print("LeftDist ");
    // Serial.print(LeftDist);
    // Serial.print(" ");
    // Serial.print("RightDist ");
    // Serial.print(RightDist);
    // Serial.print(" ");
    // Serial.print("CenterDist ");
    // Serial.print(CenterDist);
    // Serial.print(" ");
    // Serial.print("ArcA ");
    // Serial.println(ArcA);
    // Serial.print("XPosExt ");
    // Serial.print(XPosExt);
    // Serial.print(" ");
    // Serial.print("YPosExt ");
    // Serial.print(YPosExt);
    // Serial.print(" ");
    // Serial.print("APosExt ");
    // Serial.println(APosExt);
    LeftDist = (encoderValueL-YPosExt/MM_PER_CNT_EXT)*MM_PER_CNT_EXT;
    RightDist = (encoderValueR-XPosExt/MM_PER_CNT_EXT)*MM_PER_CNT_EXT;
    CenterDist = ((LeftDist+RightDist)/2)*MM_PER_CNT_EXT;
    ArcA = (RightDist-LeftDist)/WHEEL_SEP_EXT;
    XPosExt += CenterDist*cos(APosExt);
    YPosExt += CenterDist*sin(APosExt);
    APosExt += ArcA;
}

void UpdateExtEncoder()
{
    int MSB_L = digitalRead(encoderPinL1); //MSB_L = most significant bit left motor
    int LSB_L = digitalRead(encoderPinL2); //LSB_L = least significant bit left motor
    int MSB_R = digitalRead(encoderPinR1); //MSB_R = most significant bit right motor
    int LSB_R = digitalRead(encoderPinR2); //LSB_R = least significant bit right motor

    int encodedL = (MSB_L << 1) |LSB_L; //converting the 2 pin of the left motor value to single number
    int encodedR = (MSB_R << 1) |LSB_R; //converting the 2 pin of the right motor value to single number
    int sumML  = (lastEncodedL << 2) | encodedL; //adding it to the previous encoded value for left motor
    int sumMR  = (lastEncodedR << 2) | encodedR; //adding it to the previous encoded value for right motor

    if(sumML == 0b1101 || sumML == 0b0100 || sumML == 0b0010 || sumML == 0b1011) encoderValueL ++;
    if(sumML == 0b1110 || sumML == 0b0111 || sumML == 0b0001 || sumML == 0b1000) encoderValueL --;
    if(sumMR == 0b1101 || sumMR == 0b0100 || sumMR == 0b0010 || sumMR == 0b1011) encoderValueR ++;
    if(sumMR == 0b1110 || sumMR == 0b0111 || sumMR == 0b0001 || sumMR == 0b1000) encoderValueR --;

    lastEncodedL = encodedL; //store this value for next time
    lastEncodedR = encodedR;
}
