#include <Servo.h>
#include <Wire.h>

enum Instruction {
  NONE = 0x00,
  HOME = 0x01, 
  CLOSE_LATCH = 0x10,
  OPEN_LATCH = 0x11,
  CLOSE_ARM_LEFT = 0x20,
  EXTEND_ARM_LEFT = 0x21,
  CLOSE_ARM_RIGHT = 0x30,  
  EXTEND_ARM_RIGHT = 0x31,
  SORT_BALLS = 0x40,
};

enum State {
  UNINITIATED = -1,
  READY = 0,
  WORKING = 1,
  STOPPED = 2,
  DONE = 3,
};

volatile Instruction _instruction = NONE;
volatile State _state = READY;

#define LATCH_CLOSE (0)         //Latch closed
#define LATCH_OPEN (90)         //Latch opened to drop waste water
#define LEFT_ARM_CLOSED (0)
#define LEFT_ARM_EXTEND (180)
#define RIGHT_ARM_CLOSED (0)    //Where right arm is in starting position
#define RIGHT_ARM_EXTEND (180)  //Where right arm is in extended position
#define CATCH_GOOD (0)          //Position where you catch good, waste ball can fall in its hole
#define CATCH_WASTE (120)       //Position where you catch waste, good ball can fall in its hole

#define SORTER_DELAY (1000)
#define WAIT_TIME (500)

Servo latch;
Servo leftArm;
Servo rightArm;
Servo sorter;

void setup() {
  Wire.begin(0x34);
  Wire.onReceive(dataReceive);
  Wire.onRequest(dataRequest);

  latch.attach(13);
  leftArm.attach(12);
  rightArm.attach(11);
  sorter.attach(10);
}

void loop() { 
  if (_state == WORKING) {
    switch (_instruction) {
      case HOME :                       //Set everything at starting position
        latch.write(LATCH_CLOSE);
        leftArm.write(LEFT_ARM_CLOSED);
        rightArm.write(RIGHT_ARM_CLOSED);
        sorter.write(CATCH_GOOD);
        delay(WAIT_TIME);               //Wait for servos to finish moving
        _state = READY;
        break;

      case CLOSE_LATCH :                //Dump wastewater
        latch.write(LATCH_CLOSE);
        delay(WAIT_TIME);               //Wait for servo to finish moving
        _state = READY;
        break;        
      case OPEN_LATCH :                 //Close latch so wastewater can't fall
        latch.write(LATCH_OPEN);
        delay(WAIT_TIME);               //Wait for servo to finish moving
        _state = READY;
        break;
        
      case CLOSE_ARM_LEFT :             //Starting / driving position of left arm
        leftArm.write(LATCH_CLOSE);
        delay(WAIT_TIME);               //Wait for servo to finish moving
        _state = READY;
        break;        
      case EXTEND_ARM_LEFT :            //Pushing position of left arm
        leftArm.write(LATCH_CLOSE);
        delay(WAIT_TIME);               //Wait for servo to finish moving
        _state = READY;
        break;
        
      case CLOSE_ARM_RIGHT :            //Starting / Driving position of right arm  
        rightArm.write(LATCH_CLOSE);
        delay(WAIT_TIME);               //Wait for servo to finish moving
        _state = READY;
        break;        
      case EXTEND_ARM_RIGHT:            //Pushing position of right arm
        rightArm.write(LATCH_CLOSE);
        delay(WAIT_TIME);               //Wait for servo to finish moving
        _state = READY;
        break;

      case SORT_BALLS :
        for (int i = 0; i <= 3; i++) {
          sorter.write(CATCH_WASTE);    //Drop good ball and catch waste ball
          delay(SORTER_DELAY);          //Wait for next ball to fall
          sorter.write(CATCH_GOOD);     //Drop waste ball and catch good ball...
          delay(SORTER_DELAY);
        }        
    }
  }
}

void dataReceive() {
  _instruction = static_cast<Instruction>(Wire.read());
  _state = WORKING;
}

void dataRequest() {
  Wire.write(_state);
}

